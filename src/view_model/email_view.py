from database.model import EmailInformation


class EmailView:
    """
    Represents an Email Information for the view model.
    """

    def __init__(self, email=None, dict=None):
        if isinstance(email, EmailInformation):
            self.id = email.id
            self.email_address = email.email_address

        elif dict is not None:
            if 'id' in dict:
                self.id = dict['id']

            self.email_address = dict['email_address']

        else:
            self.id = -1
            self.email_address = ''