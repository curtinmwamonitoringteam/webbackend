from database.model import Tile


class TileView:
    """
    Represents a Tile for the view model.
    """

    def __init__(self, tile=None, dict=None):
        if isinstance(tile, Tile):
            self.id = tile.id
            self.name = tile.name

        elif dict is not None:
            if 'id' in dict:
                self.id = dict['id']

            self.receiver_number = dict['name']

        else:
            self.id = -1
            self.name = ''
