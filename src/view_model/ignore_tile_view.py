from database.model import IgnoreTile


class IgnoreTileView:
    """
    Represents an Ignored Tile for the view model.
    """

    def __init__(self, ignore_tile=None, dict=None):
        if isinstance(ignore_tile, IgnoreTile):
            self.id = ignore_tile.id
            self.tile_id = ignore_tile.tile_id
        elif dict is not None:
            if 'id' in dict:
                self.id = dict['id']

            self.tile_id = dict['tile_id']
        else:
            self.id = -1
            self.tile_id = -1
