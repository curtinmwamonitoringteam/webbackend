-- USAGE: mysql < "PATH_TO_FILE\test_database_setup.sql"

-- delete the mwa_test database if existent
DROP DATABASE IF EXISTS mwa_test;

-- create the mwa_test database
CREATE DATABASE mwa_test;

-- drop 'mwa_test_user' user
DROP USER IF EXISTS 'mwa_test_user'@'localhost';

-- create the development user 'mwa_test_user' identified by the password 'mwa_test_pwd'
CREATE USER 'mwa_test_user'@'localhost' IDENTIFIED BY 'mwa_test_pwd';

-- verify user creation
-- select host, user from mysql.user;

-- verify user privileges
-- SELECT host, user, select_priv, insert_priv, update_priv, delete_priv, create_priv, alter_priv FROM mysql.user WHERE user='mwa_test_user';

-- grant full privileges to the user, for easy development
GRANT ALL PRIVILEGES ON *.* TO 'mwa_test_user'@'localhost';

-- verify privileges set
-- SELECT host, user, select_priv, insert_priv, update_priv, delete_priv, create_priv, alter_priv FROM mysql.user WHERE user='mwa_test_user';
