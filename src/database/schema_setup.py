import os, sys

from sqlalchemy import create_engine

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from app.config import app_config
from database.model import Base

# Engine to handle db connection
engine = create_engine(app_config[os.getenv('APP_SETTINGS')].DATABASE_URI)

# Create/update schema on the database
Base.metadata.create_all(engine)
