-- USAGE: mysql < "PATH_TO_FILE\prod_database_setup.sql"

-- delete the mwamonitor database if existent
DROP DATABASE IF EXISTS mwamonitor;

-- create the mwamonitor database
CREATE DATABASE mwamonitor;

-- drop 'mwauser' user
DROP USER IF EXISTS 'mwauser'@'localhost';

-- create the development user 'mwauser' identified by the password 'mwaicrar'
CREATE USER 'mwauser'@'localhost' IDENTIFIED BY 'mwaicrar';

-- verify user creation
-- select host, user from mysql.user;

-- verify user privileges
-- SELECT host, user, select_priv, insert_priv, update_priv, delete_priv, create_priv, alter_priv FROM mysql.user WHERE user='mwauser';

-- grant full privileges to the user, for easy development
GRANT ALL PRIVILEGES ON *.* TO 'mwauser'@'localhost';

-- verify privileges set
-- SELECT host, user, select_priv, insert_priv, update_priv, delete_priv, create_priv, alter_priv FROM mysql.user WHERE user='mwauser';
