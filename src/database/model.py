from sqlalchemy import Column, Integer, String, ForeignKey, Table
from sqlalchemy.dialects.mysql import JSON, BIT
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()

# Association object
TestObservation = Table(
    'test_observation',
    Base.metadata,
    Column('test_id', ForeignKey('test.id', ondelete='CASCADE'), primary_key=True, index=True),
    Column('observation_id', ForeignKey('observation.id', ondelete='CASCADE'), primary_key=True, index=True)
)


# Model tables
class Anomaly(Base):
    """
    Represents an Anomaly table within the database. Inherits from Base within the SQL API.
    """
    __tablename__ = 'anomaly'
    id = Column(Integer, primary_key=True, autoincrement=True)
    type = Column(Integer, ForeignKey('anomaly_type.id', ondelete='CASCADE'), nullable=False)
    observation = Column(Integer, ForeignKey('observation.id', ondelete='CASCADE'), nullable=False)

    def to_dict(self):
        """
        Converts this Anomaly entry into a dictionary.

        Returns:
            anomaly (dict): A dictionary representation of this Anomaly.
        """
        return {
            'id': self.id,
            'type': self.type,
            'observation': self.observation
        }


class AnomalyType(Base):
    """
    Represents an AnomalyType table within the database. Inherits from Base within the SQL API.
    """
    __tablename__ = 'anomaly_type'
    id = Column(Integer, primary_key=True, autoincrement=False)
    name = Column(String(40), nullable=False)
    description = Column(String(400))

    anomaly = relationship('Anomaly', cascade='all, delete-orphan')

    def to_dict(self):
        """
        Converts this AnomalyType entry into a dictionary.

        Returns:
            anomaly_type (dict): A dictionary representation of this AnomalyType.
        """
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description
        }


class Dipole(Base):
    """
    Represents a Dipole table within the database. Inherits from Base within the SQL API.
    """
    __tablename__ = 'dipole'
    id = Column(Integer, primary_key=True, autoincrement=True)
    tile_id = Column(Integer, ForeignKey('tile.id', ondelete='CASCADE'), nullable=False)
    dipole_number = Column(Integer, nullable=False)
    polarity = Column(BIT, nullable=False)  # 0 = X, 1 = Y

    observation = relationship('Observation', cascade='all, delete-orphan')

    def to_dict(self):
        """
        Converts this Dipole entry into a dictionary.

        Returns:
            dipole (dict): A dictionary representation of this Dipole.
        """
        return {
            'id': self.id,
            'tile_id': self.tile_id,
            'dipole_number': self.dipole_number,
            'polarity': self.polarity
        }


class Tile(Base):
    """
    Represents a Tile table within the database. Inherits from Base within the SQL API.
    """
    __tablename__ = 'tile'
    id = Column(Integer, primary_key=True, autoincrement=False)
    name = Column(String(20), nullable=False)

    dipole = relationship('Dipole', cascade='all, delete-orphan')
    test = relationship('Test', cascade='all, delete-orphan')

    def to_dict(self):
        """
        Converts this Tile entry into a dictionary.

        Returns:
            tile (dict): A dictionary representation of this Tile.
        """
        return {
            'id': self.id,
            'name': self.name
        }


class Test(Base):
    """
    Represents a Test table within the database. Inherits from Base within the SQL API.
    """
    __tablename__ = 'test'
    id = Column(Integer, primary_key=True, autoincrement=True)
    tile_id = Column(Integer, ForeignKey('tile.id', ondelete='CASCADE'), nullable=False)
    start_time = Column(Integer, nullable=False)

    observations = relationship(
        'Observation',
        secondary=TestObservation,
        cascade='all, delete-orphan',
        single_parent=True
    )

    def to_dict(self):
        """
        Converts this Test entry into a dictionary.

        Returns:
            test (dict): A dictionary representation of this Test.
        """
        return {
            'id': self.id,
            'tile_id': self.tile_id,
            'start_time': self.start_time,
            'observations': self.observations
        }


class Observation(Base):
    """
    Represents an Observation table within the database. Inherits from Base within the SQL API.
    """
    __tablename__ = 'observation'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(20), nullable=False)
    start_time = Column(Integer, nullable=False)
    dipole_id = Column(Integer, ForeignKey('dipole.id', ondelete='CASCADE'), nullable=False)
    values = Column(JSON, nullable=False)

    anomaly = relationship('Anomaly', cascade='all, delete-orphan')

    def to_dict(self):
        """
        Converts this Observation entry into a dictionary.

        Returns:
            observation (dict): A dictionary representation of this Observation.
        """
        return {
            'id': self.id,
            'name': self.name,
            'start_time': self.start_time,
            'dipole_id': self.dipole_id,
            'values': self.values
        }


class IgnoreTile(Base):
    """
    Represents an Ignored Tile table within the database. Inherits from Base within the SQL API.
    """
    __tablename__ = 'ignore_tile'
    id = Column(Integer, primary_key=True, autoincrement=True)
    tile_id = Column(Integer, ForeignKey('tile.id', ondelete='CASCADE'), nullable=False)


class EmailInformation(Base):
    """
    Represents e-mail information within the database. Inherits from Base within the SQL API.
    """
    __tablename__ = 'email_information'
    id = Column(Integer, primary_key=True, autoincrement=True)
    email_address = Column(String(254), nullable=False)

    def to_dict(self):
        """
        Converts this Email Information entry into a dictionary

        Returns:
            email_information (dict): A dictionary representation of this Email Information.
        """
        return {
            'id': self.id,
            'email_address': self.email_address
        }