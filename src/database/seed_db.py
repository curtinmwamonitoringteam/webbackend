import os, sys

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from app.config import app_config
from database.model import Tile, Dipole, AnomalyType, IgnoreTile

# Connection handler
engine = create_engine(app_config[os.getenv('APP_SETTINGS')].DATABASE_URI)

# Create new Session
session = sessionmaker(bind = engine)()

# seed tiles
# initial 128 tiles
for receiver in range(1, 17):
	for slot in range(1, 9):
		session.add(Tile(
				id = str(receiver) + str(slot),
				name = 'Tile' + str(receiver).zfill(2) + str(slot)
		))
# 72 hex tiles
# east hex
for tile_id in range(1001, 1037):
	session.add(Tile(
			id = tile_id,
			name = 'HexE' + str(tile_id % 1000)
	))
# south hex
for tile_id in range(1037, 1073):
	session.add(Tile(
			id = tile_id,
			name = 'HexS' + str((tile_id % 1000) - 36)
	))
# 56 long baseline tiles
tile_id = 2001
for receiver_letter in [chr(ii) for ii in range(ord('A'), ord('H'))]:
	for slot in range(1, 9):
		session.add(Tile(
				id = tile_id,
				name = 'LB' + receiver_letter + str(slot)
		))
		tile_id += 1
# Engineering Development Array
session.add(Tile(
		id = 99,
		name = 'EDA'
))
# AAVS0.5
session.add(Tile(
		id = 79,
		name = 'AAVS0.5'
))

session.commit()

# seed ignored tiles
session.add(IgnoreTile(tile_id = 99))
session.add(IgnoreTile(tile_id = 79))

session.commit()

# Seed dipoles
tiles = session.query(Tile)

for tile in tiles:
	for dipole in range(20):
		session.add(Dipole(tile_id = tile.id, dipole_number = dipole, polarity = 0))
		session.add(Dipole(tile_id = tile.id, dipole_number = dipole, polarity = 1))

session.commit()

# seed anomaly types
session.add(AnomalyType(
		id = 1,
		name = 'Rainbow Effect',
		description = 'Beam Former: Rainbow effect indicates an issue with the beam former.'))
session.add(AnomalyType(
		id = 2,
		name = 'Power While Off',
		description = 'Beam Former: Indicates that something (possibly the beam former) is broken.'))
session.add(AnomalyType(
		id = 3,
		name = 'Clustered Lines',
		description = 'Beam Former: Clustered lines indicates an issue with the beam former unable to manage power output.'))

session.add(AnomalyType(
		id = 4,
		name = 'Flipped Cables',
		description = 'Flipped Cables: Dipoles stronger on the X axis than the Y axis for frequencies around 250 MHz.'))

session.add(AnomalyType(
		id = 5,
		name = 'All Black',
		description = 'Maintenance: Tile offline, possibly for maintenance or something is broken.'))
session.add(AnomalyType(
		id = 6,
		name = 'Off Dipole',
		description = 'Maintenance: Dipole offline, possibly for maintenance or something is broken.'))
session.add(AnomalyType(
		id = 7,
		name = 'Lower Power',
		description = 'Electronics: Indicates rain and is caused by dust on the amplifier mixing with water'
					  'and turning acidic.'))
session.add(AnomalyType(
		id = 8,
		name = 'RF Pollution',
		description = 'Digital: Radio is being flushed out by RF or something else.'
					  'Could possibly be an issue with the beam-former or dipole.'))
session.add(AnomalyType(
		id = 9,
		name = 'Dipole Deviates',
		description = 'Dipole: Dipole has more or less power than other dipoles on the same tile over time.'))
session.add(AnomalyType(
		id = 10,
		name = 'Dipole Drifts',
		description = 'Dipole: Dipole slowly deviates from the curve over time.'))
session.add(AnomalyType(
		id = 11,
		name = '0s in dipole',
		description = 'Dipole: Dipole values have not been read properly from MWA database.'))
session.add(AnomalyType(
		id = 12,
		name = 'Invalid Tile',
		description = 'Tile: Values are much higher than what is expected'))

session.commit()
