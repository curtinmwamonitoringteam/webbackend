import json

from flask import Response

from repository import dipole_repository as repo
from view_model.dipole_view import DipoleView


def get_dipole_all():
    """
    Get all Dipoles.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    dipoles = repo.get_all()
    data = []

    for dipole in dipoles:
        data.append(DipoleView(dipole=dipole).__dict__)

    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp


def get_dipole_by_id(dipole_id):
    """
    Get a single Dipole that has a matching ID.

    Args:
        dipole_id (int): ID of a dipole.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    dipole = repo.get_by_id(dipole_id)

    if not dipole:
        resp.status_code = 404
    else:
        data = DipoleView(dipole=dipole).__dict__

        resp.data = json.dumps(data)
        resp.mimetype = 'application/json'
        resp.status_code = 200

    return resp


def get_dipoles_by_tile_id(tile_id):
    """
    Get all Dipoles with matching IDs from the given list.

    Args:
        tile_id (int): ID of a tile.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    dipoles = repo.get_by_tile_id(tile_id)
    data = []

    for di in dipoles:
        data.append(DipoleView(dipole=di).__dict__)

    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp
