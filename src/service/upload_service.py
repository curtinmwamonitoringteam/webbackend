import os
from cPickle import UnpicklingError

from flask import Response
from werkzeug.utils import secure_filename

from app import app_config
from service import email_service as email
from processor.parsing.dptest import load_file
from processor.parsing.parser import parse_all_data
from processor import analyzer
from repository import test_repository as test_repo
from repository import anomaly_repository as anon_repo
from repository import observation_repository as obs_repo
from repository import dipole_repository as dip_repo


def upload_file(request):
    """
    Serves a file upload request, starts processing the file if it is valid,
    removes the file before the request returns.

    Args:
        request (Request): A web request containing a status code and data.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the resulting output.
    """
    resp = Response()

    # Get the file
    uploaded_file = request.files['file']

    # If file is valid
    if uploaded_file and uploaded_file.filename != '' and allowed_file(uploaded_file.filename):
        # Sanitize file name
        filename = secure_filename(uploaded_file.filename)

        # Save file
        file_path = os.path.join(
            app_config[os.getenv('APP_SETTINGS')].UPLOAD_FOLDER,
            filename)

        uploaded_file.save(file_path)

        # Start processing file
        try:
            process_pickle_file(file_path)

            email.send_emails()
            resp.status_code = 200
            resp.data = "Sent emails to all subscribers."
        except Exception as ex:
            resp.status_code = 400
            resp.data = ex

        # Remove file from disk
        uploaded_file.close()
        os.remove(file_path)
    else:
        resp.status_code = 400
        resp.data = 'File is invalid.'

    return resp


def allowed_file(filename):
    """
    Checks file extension against set of allowed extensions.

    Args:
        filename (str): The provided file name and extension.

    Returns:
        filename (str): A valid representation of the given file name.
    """
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in \
           app_config[os.getenv('APP_SETTINGS')].ALLOWED_EXTENSIONS


def process_pickle_file(filename):
    """
    Attempts to unpickle filename, parse its data into the database models,
    do anomaly detection, and post data and anomaly detection results,
    to the database.

    Args:
        filename (str): The provided file name and extension.
    """
    # Unpickle filename
    all_data, dstate = load_file(filename)

    # Parse to database models
    test_list = parse_all_data(all_data)

    # Push to database
    test_repo.create_all(test_list)

    # Get todays test list
    tests = test_repo.get_by_start_time(test_list[0].start_time, True)

    # Get anomalies in test list
    analyze = analyzer.Analyzer()
    anomalies = analyze.analyze(tests)

    # Push to database
    test_repo.create_all(anomalies)