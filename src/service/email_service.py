import json

import datetime
from flask import Response, current_app
from flask_mail import Mail, Message

from database.model import EmailInformation
from repository import email_repository as repo
from view_model.email_view import EmailView


def get_email_all():
    """
    Get all Email Information.

    Returns:
         resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    emails = repo.get_all()
    data = []

    for email in emails:
        data.append(EmailView(email=email).__dict__)

    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp


def get_by_email(email_address):
    """
    Gets a single email information.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    email = repo.get_by_email(email_address)

    if not email:
        resp.status_code = 404
    else:
        data = EmailView(email=email).__dict__

        resp.data = json.dumps(data)
        resp.mimetype = 'application/json'
        resp.status_code = 200

    return resp


def post_email(request):
    """
    Post a single Email Information.

    Args:
        request (Request): Represents a web request.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()

    data = EmailInformation(
        email_address=request.form['email_address']
    )
    email_id = repo.create(data)

    if email_id != 0:
        resp.data = json.dumps({
            'id': email_id,
            'email_address': request.form['email_address']
        })
        resp.mimetype = 'application/json'
        resp.status_code = 201
    else:
        resp.data = 'Conflict: 409 Conflict: The email address already exists.'
        resp.status_code = 409

    return resp


def delete_by_email_address(email_address):
    """
    Delete a single Email Information if it exists.

    Args:
        email_address (string): Email Address of an email information

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    repo.delete_by_email(email_address)
    resp.status_code = 200

    return resp


def send_emails():
    """
    Sends emails to all email addresses that exist in the database.
    """
    emails = get_email_all()

    emails = json.loads(emails.data)

    mail = Mail(current_app)

    with mail.connect() as conn:
        for email in emails:
            sender = "mwa.monitor@gmail.com"
            subject = "New Test Uploaded " + datetime.date.today().strftime("%B %d, %Y")
            message = 'Good Day, \n\n' \
                      'A new test has been uploaded to the database. It can be viewed on the website for analysis. \n\n' \
                      'Regards, \n' \
                      'MWA Admin'
            msg = Message(recipients=[email['email_address']],
                          body=message,
                          subject=subject,
                          sender=sender)

            conn.send(msg)