import json

from flask import Response

from repository import tile_repository as repo
from view_model.tile_view import TileView


def get_tile_all():
    """
    Get all Tiles.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    tiles = repo.get_all()
    data = []

    for tile in tiles:
        data.append(TileView(tile=tile).__dict__)

    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp


def get_tile_by_id(tile_id):
    """
    Get a single Tile that has a matching ID.

    Args:
        tile_id (int): ID of a tile.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    tile = repo.get_by_id(tile_id)

    if not tile:
        resp.status_code = 404

        return resp

    data = TileView(tile=tile).__dict__
    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp
