import json

from flask import Response

from database.model import Test, Observation
from repository import test_repository as repo
from view_model.observation_view import ObservationView
from view_model.test_view import TestView


def get_test_all(get_obs=False):
    """
    Get all Tests. Pass get_obs as True to return the observations associated with the test.

    Args:
        get_obs (bool): If true then return the observations associated with the test.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    tests = repo.get_all(get_obs)
    data = []

    for test in tests:
        obs_list = []

        if get_obs == 'true':
            for obs in test.observations:
                obs_list.append(ObservationView(obs=obs).__dict__)

        data.append({
            'id': test.id,
            'tile_id': test.tile_id,
            'start_time': test.start_time,
            'observations': obs_list
        })

    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp


def get_test_by_id(test_id, get_obs=False):
    """
    Get a single Test that has a matching ID.
    Pass get_obs as True to return the observations associated with the test.

    Args:
        test_id (int): ID of a test.
        get_obs (bool): If true then return the observations associated with the test.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    test = repo.get_by_id(test_id, get_obs)

    if not test:
        resp.status_code = 404
    else:
        obs_list = []

        if get_obs == 'true':
            for obs in test.observations:
                obs_list.append(ObservationView(obs=obs).__dict__)

        data = {
            'id': test.id,
            'tile_id': test.tile_id,
            'start_time': test.start_time,
            'observations': obs_list
        }

        resp.data = json.dumps(data)
        resp.mimetype = 'application/json'
        resp.status_code = 200

    return resp


def post_test(request):
    """
    Post a single Test.

    Args:
        request (Request): Represents a web request.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()

    # Build list of observations
    obs_list = []

    for obs in json.loads(request.form['observations']):
        obs_list.append(Observation(
            name=obs['name'],
            start_time=obs['start_time'],
            dipole_id=obs['dipole_id'],
            values=obs['values'],
        ))

    # Build test object
    data = Test(
        tile_id=request.form['tile_id'],
        start_time=request.form['start_time'],
        observations=obs_list
    )
    test_id = repo.create(data)

    resp.data = json.dumps({'id': test_id})
    resp.mimetype = 'application/json'
    resp.status_code = 201

    return resp


def put_test_by_id(request, test_id):
    """
    Put a single Test. Does not update observations.

    Args:
        request (Request): Represents a web request.
        test_id (int): ID of a test.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    query = repo.get_by_id(test_id)
    resp = Response()

    if not query:
        # Create the Test if it doesn't exist and return its id
        resp = post_test(request)
    else:
        # Update Test
        data = TestView(dict=request.form)
        data.id = test_id
        repo.update(data)
        resp.status_code = 200

    return resp


def delete_test_by_id(test_id):
    """
    Delete a single Test if it exists.

    Args:
        test_id (int): ID of a test.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()

    repo.delete_by_id(test_id)
    resp.status_code = 200

    return resp


def get_test_by_start_time(start_time, get_obs=False):
    """
    Get all tests with start_time == time.

    Args:
        start_time (int): The tests starting time.
        get_obs (bool): If true then return the observations associated with the test.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    tests = repo.get_by_start_time(start_time, get_obs)

    data = []

    for test in tests:
        obs_list = []
        if get_obs == 'true':
            for obs in test.observations:
                obs_list.append(ObservationView(obs=obs).__dict__)

        data.append({
            'id': test.id,
            'tile_id': test.tile_id,
            'start_time': test.start_time,
            'observations': obs_list
        })

    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp


def get_tests_in_range(start_time, end_time, get_obs = False):
    """
    Gets all tests from start_time to end_time, inclusive.

    Args:
        start_time (int): The tests starting time.
        end_time (int): The tests ending time.
        get_obs (bool): If true then return the observations associated with the tests.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    tests = repo.get_by_start_time_range(start_time, end_time, get_obs)

    data = []
    for test in tests:
        obs_list = []
        if get_obs == 'true':
            for obs in test.observations:
                obs_list.append(ObservationView(obs=obs).__dict__)

        data.append({
            'id': test.id,
            'tile_id': test.tile_id,
            'start_time': test.start_time,
            'observations': obs_list
        })

    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp


def get_tests_in_id_range(start_id, end_id, get_obs):
    """
    Gets all tests from start_id to end_id, inclusive

    Args:
        start_id (int): The tests start id.
        end_id (int): The tests end id.
        get_obs (boolean): If true then returns the observations associated with the tests.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    tests = repo.get_by_id_range(start_id, end_id, get_obs)

    data = []
    for test in tests:
        obs_list = []
        if get_obs == 'true':
            for obs in test.observations:
                obs_list.append(ObservationView(obs=obs).__dict__)

        data.append({
            'id': test.id,
            'tile_id': test.tile_id,
            'start_time': test.start_time,
            'observations': obs_list
        })

    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp