import json

from flask import Response

from database.model import Anomaly
from repository import anomaly_repository as repo
from view_model.anomaly_view import AnomalyView


def get_anomaly_all():
    """
    Get all Anomalies.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    anomalies = repo.get_all()
    data = []

    for anomaly in anomalies:
        data.append(AnomalyView(anomaly=anomaly).__dict__)

    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp


def get_anomaly_by_id(anom_id):
    """
    Get a single Anomaly that has a matching ID.

    Args:
        anom_id (int): ID of an anomaly.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    anomaly = repo.get_by_id(anom_id)

    if not anomaly:
        resp.status_code = 404
    else:
        data = AnomalyView(anomaly=anomaly).__dict__
        resp.data = json.dumps(data)
        resp.mimetype = 'application/json'
        resp.status_code = 200

    return resp


def post_anomaly(request):
    """
    Post a single Anomaly.

    Args:
        request (Request): Represents a web request.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    data = Anomaly(
        type=request.form['type'],
        observation=request.form['observation']
    )
    anomaly_id = repo.create(data)

    resp.data = json.dumps({'id': anomaly_id})
    resp.mimetype = 'application/json'
    resp.status_code = 201

    return resp


def delete_anomaly_by_id(anom_id):
    """
    Delete a single Anomaly if it exists.

    Args:
        anom_id (int): ID of an anomaly.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()

    repo.delete_by_id(anom_id)
    resp.status_code = 200

    return resp


def get_anomaly_by_obs_id(obs_id_list):
    """
    Get all anomalies that are related to any obs in obs_id_list.

    Args:
        obs_id_list (list): A list containing observations and their ID's.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    anomalies = repo.get_by_obs_id_list(obs_id_list)
    data = []

    for anomaly in anomalies:
        data.append(AnomalyView(anomaly=anomaly).__dict__)

    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp
