import json

from flask import Response

from database.model import Observation
from repository import observation_repository as repo
from view_model.observation_view import ObservationView


def get_observation_all():
    """
    Get all Observations.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    observations = repo.get_all()
    data = []

    for obs in observations:
        data.append(ObservationView(obs=obs).__dict__)

    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp


def get_observation_by_id(obs_id):
    """
    Get a single Observation that has a matching ID.

    Args:
        obs_id (int): ID of an observation.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    obs = repo.get_by_id(obs_id)

    if not obs:
        resp.status_code = 404
    else:
        data = ObservationView(obs=obs).__dict__
        resp.data = json.dumps(data)
        resp.mimetype = 'application/json'
        resp.status_code = 200

    return resp


def post_observation(request):
    """
    Post a single Observation.

    Args:
        request (Request): Represents a web request.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    data = Observation(
        name=request.form['name'],
        start_time=request.form['start_time'],
        dipole_id=request.form['dipole_id'],
        values=request.form['values']
    )
    obs_id = repo.create(data)

    resp.data = json.dumps({'id': obs_id})
    resp.mimetype = 'application/json'
    resp.status_code = 201

    return resp


def put_observation_by_id(request, obs_id):
    """
    Put a single Observation.

    Args:
        request (Request): Represents a web request.
        obs_id (int): ID of an observation.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    query = repo.get_by_id(obs_id)
    resp = Response()

    if not query:
        # Create the Observation if it doesn't exist and return its id
        resp = post_observation(request)

    else:
        # Update Observation
        data = ObservationView(dict=request.form)
        data.id = obs_id
        repo.update(data)
        resp.status_code = 200

    return resp


def delete_observation_by_id(obs_id):
    """
    Delete a single Observation if it exists.

    Args:
        obs_id (int): ID of an observation.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()

    repo.delete_by_id(obs_id)
    resp.status_code = 200

    return resp
