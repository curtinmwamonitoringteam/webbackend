import json

from flask import Response

from database.model import IgnoreTile
from repository import ignore_tile_repository as repo
from view_model.ignore_tile_view import IgnoreTileView


def get_ignore_tile_all():
    """
    Get all ignored tiles.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    ignored = repo.get_all()

    data = []
    for ignore in ignored:
        data.append(IgnoreTileView(ignore_tile=ignore).__dict__)

    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp


def post_ignore_tile(request):
    """
    Post a single IgnoreTile.

    Args:
        request (Request): Represents a web request.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    data = IgnoreTile(
        tile_id=request.form['tile_id']
    )
    ignore_tile_id = repo.create(data)

    resp.data = json.dumps({'id': ignore_tile_id})
    resp.mimetype = 'application/json'
    resp.status_code = 201

    return resp


def delete_ignore_tile(ignore_id):
    """
    Delete a single IgnoreTile if it exists.

    Args:
        ignore_id (int): ID of an ignore tile.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()

    repo.delete_by_id(ignore_id)
    resp.status_code = 200

    return resp
