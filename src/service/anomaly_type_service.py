import json

from flask import Response

from database.model import AnomalyType
from repository import anomaly_type_repository as repo
from view_model.anomaly_type_view import AnomalyTypeView


def get_anomaly_type_all():
    """
    Get all AnomalyTypes.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    anomaly_types = repo.get_all()

    data = []
    for anomaly_type in anomaly_types:
        data.append(AnomalyTypeView(anom_type=anomaly_type).__dict__)

    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp


def get_anomaly_type_by_id(type_id):
    """
    Get a single AnomalyType that has a matching ID.

    Args:
        type_id (int): ID of a tile.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    anomaly_type = repo.get_by_id(type_id)

    if not anomaly_type:
        resp.status_code = 404
    else:
        data = AnomalyTypeView(anom_type=anomaly_type).__dict__
        resp.data = json.dumps(data)
        resp.mimetype = 'application/json'
        resp.status_code = 200

    return resp


def post_anomaly_type(request):
    """
    Post a single AnomalyType.

    Args:
        request (Request): Represents a web request.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    data = AnomalyType(
        id=request.form['id'],
        name=request.form['name'],
        description=request.form['description']
    )
    repo.create(data)

    resp.mimetype = 'application/json'
    resp.status_code = 201

    return resp


def put_anomaly_type_by_id(request, type_id):
    """
    Put a single AnomalyType.

    Args:
        request (Request): Represents a web request.
        type_id (int): ID of an anomaly type.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    query = repo.get_by_id(type_id)
    resp = Response()

    if not query:
        # Create the AnomalyType if it doesn't exist
        resp = post_anomaly_type(request)

    else:
        # Update AnomalyType
        data = AnomalyTypeView(dict=request.form)
        data.id = type_id
        repo.update(data)
        resp.status_code = 200

    return resp


def delete_anomaly_type_by_id(type_id):
    """
    Delete a single AnomalyType if it exists.

    Args:
        type_id (int): ID of an anomaly type.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()

    repo.delete_by_id(type_id)
    resp.status_code = 200

    return resp


def get_anomaly_type_by_id_list(type_id_list):
    """
    Get all AnomalyTypes with matching IDs from the given list.

    Args:
        type_id_list (int): A list containing observations and their ID's.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.
    """
    resp = Response()
    types = repo.get_by_id_list(type_id_list)

    data = []
    for anom_type in types:
        data.append(AnomalyTypeView(anom_type=anom_type).__dict__)

    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp
