import json

from sqlalchemy import create_engine

from app.config import app_config
from database.model import Tile, Dipole, Observation, AnomalyType, Anomaly, Base, Test, IgnoreTile, EmailInformation


def database_set_up(app):
    """
    Put test data in the test database.

    Database must exist with schema (search config.py and schema_setup.py).
    Database does not need to be seeded (seed_db.py).

    Args:
        app (obj): Represents an instance of this application.
    """

    session=app.session_factory()

    # Build and add a Tile.
    session.add(Tile(
        id=1,
        name="test tile name"
    ))
    session.flush()

    # Build and add a Dipole.
    session.add(Dipole(
        tile_id=session.query(Tile).first().id,
        dipole_number=1,
        polarity=1
    ))
    session.flush()

    # Build an Observation
    obs = Observation(
        name='obs_name',
        start_time=1234567890,
        dipole_id=session.query(Dipole).first().id,
        values=json.dumps([1.1, 2.2, 3.3, 4.4])
    )
    session.flush()

    # Build and add a Test
    session.add(Test(
        tile_id=session.query(Tile).first().id,
        start_time=22,
        observations=[obs]
    ))
    session.flush()

    # Build and add an AnomalyType
    session.add(AnomalyType(
        id=333,
        name='test anomaly type',
        description='some words'
    ))
    session.flush()

    # Build and add an Anomaly
    session.add(Anomaly(
        type=session.query(AnomalyType).first().id,
        observation=session.query(Observation).first().id
    ))
    session.flush()

    # Build and add an IgnoreTile
    session.add(IgnoreTile(
        tile_id=session.query(Tile).first().id,
    ))
    session.flush()

    # Build and add Email Information
    session.add(EmailInformation(
        email_address="test@test.mwa.org"
    ))

    session.commit()


def database_tear_down(app):
    """
    Empty the test database.

    Args:
        app (obj): Represents an instance of this application.
    """
    session = app.session_factory()

    # Truncate database, keeping seeded Tiles and Dipoles
    session.query(Tile).delete()
    session.query(Dipole).delete()
    session.query(Observation).delete()
    session.query(Test).delete()
    session.query(AnomalyType).delete()
    session.query(Anomaly).delete()
    session.query(IgnoreTile).delete()
    session.query(EmailInformation).delete()

    session.commit()


def schema_set_up():
    """
    Create the schema on the test database.
    """
    engine = create_engine(app_config['test'].DATABASE_URI)
    Base.metadata.create_all(engine)


if __name__ == "__main__":
    schema_set_up()
