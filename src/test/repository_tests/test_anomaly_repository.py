import unittest

import repository.anomaly_repository as repo
from app import create_app
from database.model import AnomalyType, Observation, Anomaly
from test.setup_test import database_set_up, database_tear_down


class AnomalyRepositoryTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the anomaly repository class.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def test_get_all(self):
        """
        Test getting all Anomalies.
        """
        session = self.app.session_factory()

        expected = session.query(Anomaly).count()
        session.close()

        results = len(repo.get_all())

        self.assertEquals(results, expected)

    def test_get_by_id(self):
        """
        Test getting a single Anomaly with a matching ID.
        """
        session = self.app.session_factory()

        expected = session.query(Anomaly).first()
        session.close()

        results = repo.get_by_id(expected.id)

        # ID's match
        self.assertEquals(results.id, expected.id)

        # Check if fails when not passed an int
        with self.assertRaises(TypeError):
            repo.get_by_id('foo')

    def test_create(self):
        """
        Test creating an Anomaly, and getting it's associated row ID.
        """
        session = self.app.session_factory()
        before_count = session.query(Anomaly).count()
        type_id = session.query(AnomalyType).first().id
        obs_id = session.query(Observation).first().id
        session.close()

        repo.create(Anomaly(type=type_id, observation=obs_id))

        session = self.app.session_factory()
        after_count = session.query(Anomaly).count()
        session.close()

        self.assertLess(before_count, after_count)

        with self.assertRaises(TypeError):
            repo.create('foo')

    def test_delete_by_id(self):
        """
        Test deleting an Anomaly given an ID.
        """
        session = self.app.session_factory()

        initial_count = session.query(Anomaly).count()

        anomaly_id = session.query(Anomaly).first().id
        session.close()

        repo.delete_by_id(anomaly_id)

        session = self.app.session_factory()
        result = session.query(Anomaly).count()
        session.close()

        self.assertLess(result, initial_count)

        with self.assertRaises(TypeError):
            repo.delete_by_id('foo')

    def test_get_by_obs_id_list(self):
        """
        Test getting Anomalies associated with specific observations.
        """
        session = self.app.session_factory()

        anomaly = session.query(Anomaly).first()
        expected = str(anomaly.observation)
        session.close()

        result = repo.get_by_obs_id_list(expected)

        self.assertEquals(anomaly.id, result[0].id)
