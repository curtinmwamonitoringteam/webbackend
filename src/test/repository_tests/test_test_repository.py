import json
import unittest

import repository.test_repository as repo
from app import create_app
from database.model import Test, Tile, Observation, Dipole
from test.setup_test import database_set_up, database_tear_down


class TestRepositoryTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the test repository class.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def test_get_all(self):
        """
        Test getting all Tests.
        """
        session = self.app.session_factory()

        expected = session.query(Test).count()
        session.close()

        results = len(repo.get_all())

        self.assertEquals(results, expected)

    def test_get_by_id(self):
        """
        Test getting a single Test with a matching ID.
        """
        session = self.app.session_factory()

        expected = session.query(Test).first()
        session.close()

        results = repo.get_by_id(expected.id)

        # ID's match
        self.assertEquals(results.id, expected.id)

        # Check if fails when not passed an int.
        with self.assertRaises(TypeError):
            repo.get_by_id('foo')

    def test_create(self):
        """
        Test creating a Test, and getting it's associated row ID.
        """
        session = self.app.session_factory()

        obs = Observation(
            name='obs_name_create',
            start_time=1234567890,
            dipole_id=session.query(Dipole).first().id,
            values=json.dumps([1.2, 2.3, 3.4, 4.5])
        )

        test = Test()

        test.tile_id = session.query(Tile).first().id
        test.start_time = 24
        test.observations = [obs]

        row_id = session.query(Test).first().id
        before_count = session.query(Test).count()
        session.close()

        result_row_id = repo.create(test)

        session = self.app.session_factory()
        after_count = session.query(Test).count()
        session.close()

        self.assertLess(before_count, after_count)
        self.assertNotEquals(row_id, result_row_id)

        with self.assertRaises(TypeError):
            repo.create('foo')

    def test_create_all(self):
        """
        Test creating all tests in a list.
        """
        session = self.app.session_factory()

        obs = Observation(
            name='obs_name_create_all',
            start_time=1234567890,
            dipole_id=session.query(Dipole).first().id,
            values=json.dumps([1.2, 2.3, 3.4, 4.5])
        )

        test = Test()
        test.tile_id = session.query(Tile).first().id
        test.start_time = 24
        test.observations = [obs]
        tests = [test]

        before_count = session.query(Test).count()
        session.close()

        repo.create_all(tests)

        session = self.app.session_factory()
        after_count = session.query(Test).count()
        session.close()

        self.assertLess(before_count, after_count)

    @unittest.skip("Update test yet to be implemented.")
    def test_update(self):
        """
        Test updating a Test.
        """

    def test_delete_by_id(self):
        """
        Test deleting a Test given an ID.
        """
        session = self.app.session_factory()

        initial_count = session.query(Test).count()
        test_id = session.query(Test).first().id
        session.close()

        repo.delete_by_id(test_id)

        session = self.app.session_factory()
        result = session.query(Test).count()
        session.close()

        self.assertLess(result, initial_count)

        with self.assertRaises(TypeError):
            repo.delete_by_id('foo')
