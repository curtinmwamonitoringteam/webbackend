import json
import unittest

import repository.observation_repository as repo
from app import create_app
from database.model import Observation, Dipole
from test.setup_test import database_set_up, database_tear_down


class ObservationRepositoryTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the observation repository class.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def test_get_all(self):
        """
        Test getting all Observations.
        """
        session = self.app.session_factory()

        expected = session.query(Observation).count()
        session.close()

        results = len(repo.get_all())

        self.assertEquals(results, expected)

    def test_get_by_id(self):
        """
        Test getting a single Observation with a matching ID.
        """
        session = self.app.session_factory()

        expected = session.query(Observation).first()
        session.close()

        results = repo.get_by_id(expected.id)

        # ID's match
        self.assertEquals(results.id, expected.id)

        # Check if fails when not passed an int
        with self.assertRaises(TypeError):
            repo.get_by_id('foo')

    def test_create(self):
        """
        Test creating an Observation, and getting it's associated row ID.
        """
        session = self.app.session_factory()

        observation = Observation()

        observation.name = 'obs_name_create'
        observation.start_time = 1234567890
        observation.dipole_id = session.query(Dipole).first().id
        observation.values = json.dumps([1.2, 2.3, 3.4, 4.5])

        row_id = session.query(Observation).first().id
        before_count = session.query(Observation).count()
        session.close()

        result_row_id = repo.create(observation)

        session = self.app.session_factory()
        after_count = session.query(Observation).count()
        session.close()

        self.assertLess(before_count, after_count)
        self.assertNotEquals(row_id, result_row_id)

        with self.assertRaises(TypeError):
            repo.create('foo')

    @unittest.skip("Update observation yet to be implemented.")
    def test_update(self):
        """
        Test updating an Observation.
        """

    def test_delete_by_id(self):
        """
        Test deleting an Observation given an ID.
        """
        session = self.app.session_factory()

        initial_count = session.query(Observation).count()
        observation_id = session.query(Observation).first().id
        session.close()

        repo.delete_by_id(observation_id)

        session = self.app.session_factory()
        result = session.query(Observation).count()
        session.close()

        self.assertLess(result, initial_count)

        with self.assertRaises(TypeError):
            repo.delete_by_id('foo')
