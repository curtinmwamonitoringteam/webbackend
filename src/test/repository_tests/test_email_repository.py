import json
import unittest

import repository.email_repository as repo
from app import create_app
from database.model import EmailInformation
from test.setup_test import database_set_up, database_tear_down


class EmailRepositoryTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the email repository class.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def test_get_all(self):
        """
        Test getting all Email Information
        """
        session = self.app.session_factory()

        expected = session.query(EmailInformation).count()
        session.close()

        results = len(repo.get_all())

        self.assertEquals(results, expected)

    def test_get_by_email(self):
        """
        Test getting a single Email Information with a matching email address.
        """
        session = self.app.session_factory()

        expected = session.query(EmailInformation).first()
        session.close()

        results = repo.get_by_email(expected.email_address)

        self.assertEquals(results.email_address, expected.email_address)

    def test_create(self):
        """
        Test creating an Email Information, and getting it's associated row ID.
        """
        session = self.app.session_factory()

        email = EmailInformation()

        email.email_address = 'test@email.com'

        row_id = session.query(EmailInformation).first().id
        before_count = session.query(EmailInformation).count()
        session.close()

        result_row_id = repo.create(email)

        session = self.app.session_factory()
        after_count = session.query(EmailInformation).count()
        session.close()

        self.assertLess(before_count, after_count)
        self.assertNotEquals(row_id, result_row_id)

    def test_delete_by_email(self):
        """
        Test deleting an Email Information given an email address.
        """
        session = self.app.session_factory()

        initial_count = session.query(EmailInformation).count()
        email = session.query(EmailInformation).first().email_address
        session.close()

        repo.delete_by_email(email)

        session = self.app.session_factory()
        result = session.query(EmailInformation).count()
        session.close()

        self.assertLess(result, initial_count)
