import unittest

import repository.ignore_tile_repository as repo
from app import create_app
from database.model import IgnoreTile, Tile
from test.setup_test import database_set_up, database_tear_down


class IgnoreTileRepositoryTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the ignore tile repository class.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def test_get_all(self):
        """
        Test getting all IgnoreTile's.
        """
        session = self.app.session_factory()

        expected = session.query(IgnoreTile).count()
        session.close()

        results = len(repo.get_all())

        self.assertEquals(results, expected)

    def test_create(self):
        """
        Test creating an IgnoreTile, and getting it's associated row ID.
        """
        session = self.app.session_factory()

        ignore_tile = IgnoreTile()
        ignore_tile.tile_id = session.query(Tile).first().id

        row_id = session.query(IgnoreTile).first().id
        before_count = session.query(IgnoreTile).count()
        session.close()

        result_row_id = repo.create(ignore_tile)

        session = self.app.session_factory()
        after_count = session.query(IgnoreTile).count()
        session.close()

        self.assertLess(before_count, after_count)
        self.assertNotEquals(row_id, result_row_id)

        with self.assertRaises(TypeError):
            repo.create('foo')

    def test_delete_by_id(self):
        """
        Test deleting an IgnoreTile given an ID.
        """
        session = self.app.session_factory()

        initial_count = session.query(IgnoreTile).count()
        ignore_tile_id = session.query(IgnoreTile).first().id
        session.close()

        repo.delete_by_id(ignore_tile_id)

        session = self.app.session_factory()
        result = session.query(IgnoreTile).count()
        session.close()

        self.assertLess(result, initial_count)

        with self.assertRaises(TypeError):
            repo.delete_by_id('foo')
