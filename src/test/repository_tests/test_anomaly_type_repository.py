import unittest

import repository.anomaly_type_repository as repo
from app import create_app
from database.model import AnomalyType
from test.setup_test import database_set_up, database_tear_down


class AnomalyTypeRepositoryTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the anomaly type repository class.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def test_get_all(self):
        """
        Test getting all AnomalyTypes.
        """
        session = self.app.session_factory()

        expected = session.query(AnomalyType).count()
        session.close()

        results = len(repo.get_all())

        self.assertEquals(results, expected)

    def test_get_by_id(self):
        """
        Test getting a single AnomalyType with a matching ID.
        """
        session = self.app.session_factory()

        expected = session.query(AnomalyType).first()
        session.close()

        results = repo.get_by_id(expected.id)

        # ID's match
        self.assertEquals(results.id, expected.id)

        # Check if fails when not passed an int
        with self.assertRaises(TypeError):
            repo.get_by_id('foo')

    def test_create(self):
        """
        Test creating an AnomalyType, and getting it's associated row ID.
        """
        session = self.app.session_factory()
        before_count = session.query(AnomalyType).count()
        session.close()

        repo.create(AnomalyType(id=1, name='test', description='desc'))

        session = self.app.session_factory()
        after_count = session.query(AnomalyType).count()
        session.close()

        self.assertLess(before_count, after_count)

    @unittest.skip("Update anomaly type yet to be implemented.")
    def test_update(self):
        """
        Test updating an Anomaly.
        """

    def test_delete_by_id(self):
        """
        Test deleting an AnomalyType given an ID.
        """
        session = self.app.session_factory()

        initial_count = session.query(AnomalyType).count()
        anomaly_type_id = session.query(AnomalyType).first().id
        session.close()

        repo.delete_by_id(anomaly_type_id)

        session = self.app.session_factory()
        result = session.query(AnomalyType).count()
        session.close()

        self.assertLess(result, initial_count)

        with self.assertRaises(TypeError):
            repo.delete_by_id('foo')
