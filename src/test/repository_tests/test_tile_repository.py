import unittest

import repository.tile_repository as repo
from app import create_app
from database.model import Tile
from test.setup_test import database_set_up, database_tear_down


class TileRepositoryTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the observation repository class.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def test_get_all(self):
        """
        Test getting all Tiles.
        """
        session = self.app.session_factory()

        expected = session.query(Tile).count()
        session.close()

        results = len(repo.get_all())

        self.assertEquals(results, expected)

    def test_get_by_id(self):
        """
        Test getting a single Tile with a matching ID.
        """
        session = self.app.session_factory()

        expected = session.query(Tile).first()
        session.close()

        results = repo.get_by_id(expected.id)

        # ID's match.
        self.assertEquals(results.id, expected.id)

        # Check if fails when not passed an int.
        with self.assertRaises(TypeError):
            repo.get_by_id('foo')

    def test_create(self):
        """
        Test creating a Tile, and getting it's associated row ID.
        """
        session = self.app.session_factory()

        before_count = session.query(Tile).count()
        session.close()

        repo.create(Tile(id=999999, name='test'))

        session = self.app.session_factory()
        after_count = session.query(Tile).count()

        self.assertLess(before_count, after_count)

        # Tiles don't get cleaned up after each test case.
        repo.delete_by_id(999999)
        session.close()

    def test_update(self):
        """
        Test updating a Tile.
        """
        session = self.app.session_factory()

        tile_id = session.query(Tile).first().id
        session.close()

        repo.update(Tile(id=tile_id, name='new test name'))

        session = self.app.session_factory()
        result = session.query(Tile).first()
        session.close()

        self.assertEqual(result.name, 'new test name')

        with self.assertRaises(TypeError):
            repo.update('foo')

    def test_delete_by_id(self):
        """
        Test deleting a Tile given an ID.
        """
        session = self.app.session_factory()

        initial_count = session.query(Tile).count()
        tile_id = session.query(Tile).first().id
        session.close()

        repo.delete_by_id(tile_id)

        session = self.app.session_factory()
        result = session.query(Tile).count()
        session.close()

        self.assertLess(result, initial_count)

        with self.assertRaises(TypeError):
            repo.delete_by_id('foo')
