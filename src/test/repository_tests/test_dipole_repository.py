import unittest

import repository.dipole_repository as repo
from app import create_app
from database.model import Dipole, Tile
from test.setup_test import database_set_up, database_tear_down


class DipoleRepositoryTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the dipole repository class.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def test_get_all(self):
        """
        Test getting all Dipoles.
        """
        session = self.app.session_factory()

        expected = session.query(Dipole).count()
        session.close()

        results = len(repo.get_all())

        self.assertEquals(results, expected)

    def test_get_by_id(self):
        """
        Test getting a single Dipole with a matching ID.
        """
        session = self.app.session_factory()

        expected = session.query(Dipole).first()
        session.close()

        results = repo.get_by_id(expected.id)

        # ID's match
        self.assertEquals(results.id, expected.id)

        # Check if fails when not passed an int
        with self.assertRaises(TypeError):
            repo.get_by_id('foo')

    def test_create(self):
        """
        Test creating a Dipole, and getting it's associated row ID.
        """
        session = self.app.session_factory()
        before_count = session.query(Dipole).count()
        session.close()

        repo.create(Dipole(tile_id=1, dipole_number=4, polarity=0))

        session = self.app.session_factory()
        after_count = session.query(Dipole).count()
        session.close()

        self.assertLess(before_count, after_count)

        with self.assertRaises(TypeError):
            repo.create('foo')

    def test_delete_by_id(self):
        """
        Test deleting a Dipole given an ID.
        """
        session = self.app.session_factory()

        initial_count = session.query(Dipole).count()
        dipole_id = session.query(Dipole).first().id
        session.close()

        repo.delete_by_id(dipole_id)

        session = self.app.session_factory()
        result = session.query(Dipole).count()
        session.close()

        self.assertLess(result, initial_count)

        with self.assertRaises(TypeError):
            repo.delete_by_id('foo')

    def test_get_dipole_id(self):
        """
        Test getting a Dipole ID using a Dipoles fields.
        """
        session = self.app.session_factory()

        dipole = session.query(Dipole).first()
        session.close()

        result = repo.get_dipole_id(dipole.tile_id, dipole.dipole_number, dipole.polarity)

        self.assertEqual(result, dipole.id)

        with self.assertRaises(TypeError):
            repo.get_dipole_id('foo', 1, 1)

        with self.assertRaises(TypeError):
            repo.get_dipole_id(1, 'foo', 1)

        with self.assertRaises(TypeError):
            repo.get_dipole_id(1, 1, 'foo')

        with self.assertRaises(ValueError):
            repo.get_dipole_id(1, 1, 2)

        with self.assertRaises(ValueError):
            repo.get_dipole_id(1, 1, -1)

    def test_get_by_tile_id(self):
        """
        Test getting all Dipoles with a matching tile ID.
        """
        session = self.app.session_factory()

        tile_id = session.query(Tile).first().id
        expected = session.query(Dipole) \
            .filter(Dipole.tile_id == tile_id).count()

        session.close()

        result = repo.get_by_tile_id(tile_id)

        self.assertEqual(len(result), expected)

        with self.assertRaises(TypeError):
            repo.get_by_tile_id('foo')