import unittest

from sqlalchemy.exc import OperationalError

from app import create_app
from app.endpoint.email_endpoint import email_information, email_information_address
from database.model import EmailInformation
from test.setup_test import database_set_up, database_tear_down


class EmailServiceTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the email_service file.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def test_email_get(self):
        """
        Test '/email' endpoint using http method GET.
        """
        with self.app.test_request_context(method='GET'):
            res = email_information()
            self.assertEqual(res.status_code, 200)

    def test_email_address_get(self):
        """
        Test '/email/<string:address>' endpoint using http method GET.
        """
        session = self.app.session_factory()

        good_address = session.query(EmailInformation).first().email_address
        session.close()

        with self.app.test_request_context(method='GET'):
            res = email_information_address(email_address=good_address)
            self.assertEqual(res.status_code, 200)

        bad_address = ''

        with self.app.test_request_context(method='GET'):
            res = email_information_address(email_address=bad_address)
            self.assertEqual(res.status_code, 404)

    def test_email_post(self):
        """
        Test '/email' endpoint using http method POST.
        """
        session = self.app.session_factory()

        good_data = {
            'email_address': 'test@email.com',
            'id': 100
        }

        with self.app.test_request_context(method='POST', data=good_data):
            res = email_information()
            self.assertEqual(res.status_code, 201)

		# Duplicate
        bad_data = {
            'email_address': 'test@email.com',
            'id': 100
        }

        with self.app.test_request_context(method='POST', data=bad_data):
            res = email_information()
            self.assertEqual(res.status_code, 409)

    def test_email_delete(self):
        """
        Test '/email/<string:address>' endpoint using http method DELETE.
        """
        session = self.app.session_factory()

        delete_address = session.query(EmailInformation).first().email_address
        session.close()

        with self.app.test_request_context(method='DELETE'):
            res = email_information_address(email_address=delete_address)
            self.assertEqual(res.status_code, 200)