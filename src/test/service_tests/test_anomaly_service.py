import unittest

from sqlalchemy.exc import OperationalError
from werkzeug.exceptions import BadRequestKeyError

from app import create_app
from app.endpoint.anomaly_endpoint import anomaly, anomaly_id
from database.model import AnomalyType, Observation, Anomaly
from test.setup_test import database_set_up, database_tear_down


class AnomalyServiceTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the anomaly_service file.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def test_anomaly_get(self):
        """
        Test '/anomaly' endpoint using http method GET.
        """
        session = self.app.session_factory()

        good_data = {'obs_id_list': [
            session.query(Anomaly).first().observation
        ]}

        session.close()

        with self.app.test_request_context(method='GET'):
            res = anomaly()
            self.assertEqual(res.status_code, 200)

        with self.app.test_request_context(method='GET', query_string=good_data):
            res = anomaly()
            self.assertEqual(res.status_code, 200)

    def test_anomaly_post(self):
        """
        Test '/anomaly' endpoint using http method POST.
        """
        session = self.app.session_factory()

        good_data = {
            'type': session.query(AnomalyType).first().id,
            'observation': session.query(Observation).first().id
        }
        session.close()

        with self.app.test_request_context(method='POST', data=good_data):
            res = anomaly()
            self.assertEqual(res.status_code, 201)

        bad_key = {'bad': 4}

        with self.app.test_request_context(method='POST', data=bad_key):
            self.assertRaises(BadRequestKeyError, anomaly)

        bad_value = {'type': 'q', 'observation': -1}

        with self.app.test_request_context(method='POST', data=bad_value):
            self.assertRaises(OperationalError, anomaly)

    def test_anomaly_by_id_get(self):
        """
        Test '/anomaly/<int:id>' endpoint using http method GET.
        """
        session = self.app.session_factory()

        good_id = session.query(Anomaly).first().id
        session.close()

        with self.app.test_request_context(method='GET'):
            res = anomaly_id(anom_id=good_id)
            self.assertEqual(res.status_code, 200)

        bad_id = -1

        with self.app.test_request_context(method='GET'):
            res = anomaly_id(anom_id=bad_id)
            self.assertEqual(res.status_code, 404)

    def test_anomaly_by_id_delete(self):
        """
        Test '/anomaly/<int:id>' endpoint using http method DELETE.
        """
        with self.app.test_request_context(method='DELETE'):
            res = anomaly_id(anom_id=1)
            self.assertEqual(res.status_code, 200)
