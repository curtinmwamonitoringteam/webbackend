import unittest

from werkzeug.exceptions import BadRequestKeyError

from app import create_app
from app.endpoint.anomaly_type_endpoint import anomaly_type, anomaly_type_id
from database.model import AnomalyType
from test.setup_test import database_set_up, database_tear_down


class AnomalyTypeServiceTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the anomaly_type_service file.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def test_anomaly_type_get(self):
        """
        Test '/anomaly-type' endpoint using http method GET.
        """
        with self.app.test_request_context(method='GET'):
            res = anomaly_type()
            self.assertEqual(res.status_code, 200)

    def test_anomaly_type_post(self):
        """
        Test '/anomaly-type' endpoint using http method POST.
        """
        good_data = {'id': 999999, 'name': 'name', 'description': 'desc'}

        with self.app.test_request_context(method='POST', data=good_data):
            res = anomaly_type()
            self.assertEqual(res.status_code, 201)

        bad_key = {'bad': 4}

        with self.app.test_request_context(method='POST', data=bad_key):
            self.assertRaises(BadRequestKeyError, anomaly_type)

    def test_anomaly_type_by_id_get(self):
        """
        Test '/anomaly-type/<int:id>' endpoint using http method GET.
        """
        session = self.app.session_factory()

        good_id = session.query(AnomalyType).first().id
        session.close()

        with self.app.test_request_context(method='GET'):
            res = anomaly_type_id(type_id=good_id)
            self.assertEqual(res.status_code, 200)

        bad_id = -1

        with self.app.test_request_context(method='GET'):
            res = anomaly_type_id(type_id=bad_id)
            self.assertEqual(res.status_code, 404)

    def test_anomaly_type_by_id_put(self):
        """
        Test '/anomaly-type/<int:id>' endpoint using http method PUT.
        """
        session = self.app.session_factory()
        good_data = {'id': -1, 'name': 'name', 'description': 'desc'}
        bad_key = {'bad': 4}

        # Update.
        update_id = session.query(AnomalyType).first().id
        session.close()
        good_data['id'] = update_id

        with self.app.test_request_context(method='PUT', data=good_data):
            res = anomaly_type_id(type_id=update_id)
            self.assertEqual(res.status_code, 200)

        with self.app.test_request_context(method='PUT', data=bad_key):
            self.assertRaises(BadRequestKeyError, anomaly_type_id, update_id)

        # Create.
        good_data['id'] = create_id = 999999

        with self.app.test_request_context(method='PUT', data=good_data):
            res = anomaly_type_id(type_id=create_id)
            self.assertEqual(res.status_code, 201)

        with self.app.test_request_context(method='PUT', data=bad_key):
            self.assertRaises(BadRequestKeyError, anomaly_type_id, create_id)

    def test_anomaly_type_by_id_delete(self):
        """
        Test '/anomaly-type/<int:id>' endpoint using http method DELETE.
        """
        with self.app.test_request_context(method='DELETE'):
            res = anomaly_type_id(type_id=1)
            self.assertEqual(res.status_code, 200)
