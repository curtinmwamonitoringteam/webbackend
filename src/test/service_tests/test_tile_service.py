import unittest

from app import create_app
from app.endpoint.tile_endpoint import tile, tile_id
from database.model import Tile
from test.setup_test import database_set_up, database_tear_down


class TileServiceTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the tile_service file.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def test_tile_get(self):
        """
        Test '/tile' endpoint using http method GET.
        """
        with self.app.test_request_context(method='GET'):
            res = tile()
            self.assertEqual(res.status_code, 200)

    def test_tile_by_id_get(self):
        """
        Test '/tile/<int:id>' endpoint using http method GET.
        """
        session = self.app.session_factory()

        good_id = session.query(Tile).first().id
        session.close()

        with self.app.test_request_context(method='GET'):
            res = tile_id(tile_id=good_id)
            self.assertEqual(res.status_code, 200)

        bad_id = -1

        with self.app.test_request_context(method='GET'):
            res = tile_id(tile_id=bad_id)
            self.assertEqual(res.status_code, 404)
