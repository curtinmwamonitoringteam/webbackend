import unittest

from sqlalchemy.exc import OperationalError
from werkzeug.exceptions import BadRequestKeyError

from app import create_app
from app.endpoint.ignore_tile_endpoint import ignore_tile, ignore_tile_id
from database.model import Tile
from test.setup_test import database_set_up, database_tear_down


class IgnoreTileServiceTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the ignore_tile_service file.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def test_ignore_tile_get(self):
        """
        test '/ignore_tile' endpoint using http method GET.
        """
        with self.app.test_request_context(method='GET'):
            res = ignore_tile()
            self.assertEqual(res.status_code, 200)

    def test_ignore_tile_post(self):
        """
        Test '/ignore_tile' endpoint using http method POST.
        """
        session = self.app.session_factory()

        good_data = {
            'tile_id': session.query(Tile).first().id,
        }
        session.close()

        with self.app.test_request_context(method='POST', data=good_data):
            res = ignore_tile()
            self.assertEqual(res.status_code, 201)

        bad_key = {'bad': 4}

        with self.app.test_request_context(method='POST', data=bad_key):
            self.assertRaises(BadRequestKeyError, ignore_tile)

        bad_value = {'tile_id': 'q'}

        with self.app.test_request_context(method='POST', data=bad_value):
            self.assertRaises(OperationalError, ignore_tile)

    def test_ignore_tile_by_id_delete(self):
        """
        Test '/ignore_tile/<int:ignore_id>' endpoint using http method DELETE.
        """
        with self.app.test_request_context(method='DELETE'):
            res = ignore_tile_id(ignore_id=1)
            self.assertEqual(res.status_code, 200)
