import unittest
from io import BytesIO

from app import create_app
from app.endpoint.upload_endpoint import upload
from service.upload_service import allowed_file
from test.setup_test import database_set_up, database_tear_down


class UploadServiceTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the upload_service file.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    @unittest.skip("Test upload service needs a seeded db to process a file.")
    def test_upload_post(self):
        """
        Test '/upload' endpoint using http method POST.
        """
        good_data = dict(
            file=(BytesIO(open('sample.pickle', 'rb').read()), 'sample.pickle')
        )

        with self.app.test_request_context(method='POST', content_type='multipart/form-data', data=good_data):
            res = upload()
            self.assertEqual(res.status_code, 200)

        bad_filename = dict(
            file=(BytesIO(open('sample.pickle', 'rb').read()), 'sample.bad')
        )

        with self.app.test_request_context(method='POST', content_type='multipart/form-data', data=bad_filename):
            res = upload()
            self.assertEqual(res.status_code, 400)

        bad_file = dict(
            file=(BytesIO(b'bad file contents'), 'sample.pickle')
        )

        with self.app.test_request_context(method='POST', content_type='multipart/form-data', data=bad_file):
            res = upload()
            self.assertEqual(res.status_code, 400)

        no_file = dict()

        with self.app.test_request_context(method='POST', content_type='multipart/form-data', data=no_file):
            res = upload()
            self.assertEqual(res.status_code, 400)

    def test_allowed_file(self):
        """
        Test for allowed file extensions.

        A file with the extension '.pickle' is valid, any other is invalid.
        """
        # Allowed file extension.
        self.assertEqual(allowed_file('test.pickle'), True)

        # Disallowed file extension.
        self.assertEqual(allowed_file('test.nope'), False)
