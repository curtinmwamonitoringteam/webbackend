import json
import unittest

from sqlalchemy.exc import OperationalError
from werkzeug.exceptions import BadRequestKeyError

from app import create_app
from app.endpoint.test_endpoint import test, test_id
from database.model import Test, Tile, Observation, Dipole
from test.setup_test import database_set_up, database_tear_down


class TestServiceTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the test_service file.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def test_test_get(self):
        """
        Test '/test' endpoint using http method GET.
        """
        session = self.app.session_factory()

        query = session.query(Test).first()
        session.close()

        with self.app.test_request_context(method='GET'):
            res = test()
            self.assertEqual(res.status_code, 200)

        with self.app.test_request_context(method='GET', query_string={'get_obs': 'true'}):
            res = test()
            self.assertEqual(res.status_code, 200)

        with self.app.test_request_context(
                method='GET',
                query_string={
                    'get_obs': 'true',
                    'start_time': query.start_time
                }):
            res = test()
            self.assertEqual(res.status_code, 200)

        with self.app.test_request_context(
                method='GET',
                query_string={
                    'get_obs': 'false',
                    'start_time': query.start_time,
                    'end_time': query.start_time + 10
                }):
            res = test()
            self.assertEqual(res.status_code, 200)

    def test_test_post(self):
        """
        Test '/test' endpoint using http method POST.
        """
        session = self.app.session_factory()

        good_data = {
            'tile_id': session.query(Tile).first().id,
            'start_time': 44,
            'observations': json.dumps([
                Observation(name='some name', start_time=777888,
                            dipole_id=session.query(Dipole).first().id,
                            values=json.dumps([8.7, 8.8, 8.9])
                            ).to_dict()
            ])
        }
        session.close()

        with self.app.test_request_context(method='POST', data=good_data):
            res = test()
            self.assertEqual(res.status_code, 201)

        bad_key = {'bad': 4}

        with self.app.test_request_context(method='POST', data=bad_key):
            self.assertRaises(BadRequestKeyError, test)

        bad_value = {
            'tile_id': 'q',
            'start_time': -1,
            'observations': good_data['observations']
        }

        with self.app.test_request_context(method='POST', data=bad_value):
            self.assertRaises(OperationalError, test)

    def test_test_by_id_get(self):
        """
        Test '/test/<int:id>' endpoint using http method GET.
        """
        session = self.app.session_factory()

        good_id = session.query(Test).first().id
        session.close()

        with self.app.test_request_context(method='GET'):
            res = test_id(test_id=good_id)
            self.assertEqual(res.status_code, 200)

        with self.app.test_request_context(method='GET', query_string={'get_obs': 'true'}):
            res = test_id(test_id=good_id)
            self.assertEqual(res.status_code, 200)

        bad_id = -1

        with self.app.test_request_context(method='GET'):
            res = test_id(test_id=bad_id)
            self.assertEqual(res.status_code, 404)

    def test_test_by_id_put(self):
        """
        Test '/test/<int:id>' endpoint using http method PUT.
        """
        session = self.app.session_factory()

        good_data = {
            'tile_id': session.query(Tile).first().id,
            'start_time': 44,
            'observations': json.dumps([
                Observation(name='some name', start_time=77777,
                            dipole_id=session.query(Dipole).first().id,
                            values=json.dumps([7.7, 7.8, 7.9])
                            ).to_dict()
            ])
        }
        bad_key = {'bad': 4}
        bad_value = {
            'tile_id': 'q',
            'start_time': -1,
            'observations': good_data['observations']
        }

        # Update.
        update_id = session.query(Test).first().id
        session.close()

        with self.app.test_request_context(method='PUT', data=good_data):
            res = test_id(test_id=update_id)
            self.assertEqual(res.status_code, 200)

        with self.app.test_request_context(method='PUT', data=bad_key):
            self.assertRaises(BadRequestKeyError, test_id, update_id)

        with self.app.test_request_context(method='PUT', data=bad_value):
            self.assertRaises(OperationalError, test_id, update_id)

        # Create.
        create_id = -1

        with self.app.test_request_context(method='PUT', data=good_data):
            res = test_id(test_id=create_id)
            self.assertEqual(res.status_code, 201)

        with self.app.test_request_context(method='PUT', data=bad_key):
            self.assertRaises(BadRequestKeyError, test_id, create_id)

        with self.app.test_request_context(method='PUT', data=bad_value):
            self.assertRaises(OperationalError, test_id, create_id)

    def test_test_by_id_delete(self):
        """
        Test '/test/<int:id>' endpoint using http method DELETE.
        """
        session = self.app.session_factory()

        delete_id = session.query(Test).first().id
        session.close()

        with self.app.test_request_context(method='DELETE'):
            res = test_id(test_id=delete_id)
            self.assertEqual(res.status_code, 200)
