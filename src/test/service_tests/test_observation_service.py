import json
import unittest

from sqlalchemy.exc import OperationalError
from werkzeug.exceptions import BadRequestKeyError

from app import create_app
from app.endpoint.observation_endpoint import observation, observation_id
from database.model import Dipole, Observation
from test.setup_test import database_set_up, database_tear_down


class ObservationServiceTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the observation_service file.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def test_observation_get(self):
        """
        Test '/observation' endpoint using http method GET.
        """
        with self.app.test_request_context(method='GET'):
            res = observation()
            self.assertEqual(res.status_code, 200)

    def test_observation_post(self):
        """
        Test '/observation' endpoint using http method POST.
        """
        session = self.app.session_factory()

        good_data = {
            'name': 'test_name',
            'start_time': 12345,
            'dipole_id': session.query(Dipole).first().id,
            'values': json.dumps([1.1, 1.2, 1.3, 1.4])
        }
        session.close()

        with self.app.test_request_context(method='POST', data=good_data):
            res = observation()
            self.assertEqual(res.status_code, 201)

        bad_key = {'bad': 4}

        with self.app.test_request_context(method='POST', data=bad_key):
            self.assertRaises(BadRequestKeyError, observation)

        bad_value = {
            'name': -1,
            'start_time': 'q',
            'dipole_id': -1,
            'values': json.dumps([-1, -2.4])
        }

        with self.app.test_request_context(method='POST', data=bad_value):
            self.assertRaises(OperationalError, observation)

    def test_observation_by_id_get(self):
        """
        Test '/observation/<int:id>' endpoint using http method GET.
        """
        session = self.app.session_factory()

        good_id = session.query(Observation).first().id
        session.close()

        with self.app.test_request_context(method='GET'):
            res = observation_id(obs_id=good_id)
            self.assertEqual(res.status_code, 200)

        bad_id = -1

        with self.app.test_request_context(method='GET'):
            res = observation_id(obs_id=bad_id)
            self.assertEqual(res.status_code, 404)

    def test_observation_by_id_put(self):
        """
        Test '/observation/<int:id>' endpoint using http method PUT.
        """
        session = self.app.session_factory()

        good_data = {
            'name': 'test_name',
            'start_time': 67890,
            'dipole_id': session.query(Dipole).first().id,
            'values': json.dumps([1.1, 1.2, 1.3, 1.4])
        }
        bad_key = {'bad': 4}
        bad_value = {'name': -1, 'start_time': 'q', 'dipole_id': -1,
                     'values': json.dumps([-1, -2.4])}

        # Update.
        update_id = session.query(Observation).first().id
        session.close()

        with self.app.test_request_context(method='PUT', data=good_data):
            res = observation_id(obs_id=update_id)
            self.assertEqual(res.status_code, 200)

        with self.app.test_request_context(method='PUT', data=bad_key):
            self.assertRaises(BadRequestKeyError, observation_id, update_id)

        with self.app.test_request_context(method='PUT', data=bad_value):
            self.assertRaises(OperationalError, observation_id, update_id)

        # Create.
        create_id = -1

        with self.app.test_request_context(method='PUT', data=good_data):
            res = observation_id(obs_id=create_id)
            self.assertEqual(res.status_code, 201)

        with self.app.test_request_context(method='PUT', data=bad_key):
            self.assertRaises(BadRequestKeyError, observation_id, create_id)

        with self.app.test_request_context(method='PUT', data=bad_value):
            self.assertRaises(OperationalError, observation_id, create_id)

    def test_observation_delete(self):
        """
        Test '/observation/<int:id>' endpoint using http method DELETE.
        """
        with self.app.test_request_context(method='DELETE'):
            res = observation_id(obs_id=1)
            self.assertEqual(res.status_code, 200)
