import unittest

from app import create_app
from app.endpoint.dipole_endpoint import dipole, dipole_id
from database.model import Dipole
from test.setup_test import database_set_up, database_tear_down


class DipoleServiceTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the dipole_service file.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def test_dipole_get(self):
        """
        Test '/dipole' endpoint using http method GET.
        """
        session = self.app.session_factory()

        good_data = {'tile_id': session.query(Dipole).first().tile_id}
        session.close()

        with self.app.test_request_context(method='GET'):
            res = dipole()
            self.assertEqual(res.status_code, 200)

        with self.app.test_request_context(method='GET', query_string=good_data):
            res = dipole()
            self.assertEqual(res.status_code, 200)

    def test_dipole_by_id_get(self):
        """
        Test '/dipole/<int:id>' endpoint using http method GET.
        """
        session = self.app.session_factory()

        good_id = session.query(Dipole).first().id
        session.close()

        with self.app.test_request_context(method='GET'):
            res = dipole_id(dipole_id=good_id)
            self.assertEqual(res.status_code, 200)

        bad_id = -1

        with self.app.test_request_context(method='GET'):
            res = dipole_id(dipole_id=bad_id)
            self.assertEqual(res.status_code, 404)
