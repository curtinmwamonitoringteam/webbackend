import unittest


class ParserTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the parser file.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """

    def test_parse_all_data(self):
        """"""
