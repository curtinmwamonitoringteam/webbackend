import json
import unittest
from mock import Mock, MagicMock
from processor import analyzer

from app import create_app
from test.setup_test import database_set_up, database_tear_down


class AnalyzerTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the Analyzer class.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)


    def test_analyze_rainbow_effect(self):
        """"""
        analyze = analyzer.Analyzer()
        vals = [250 for n in range(255)]
        off_vals = [100 for n in range(221)]

        dipole1 = Mock()
        dipole1.name = "dipole1"
        dipole1.values = json.dumps(vals)

        all_off = Mock()
        all_off.name = "AllOff"
        all_off.values = json.dumps(off_vals)

        observations = MagicMock()
        observations.__iter__.return_value = [dipole1, all_off]

        expected = 1

        anomaly = analyze.analyze_rainbow_effect(observations)

        self.assertEqual(expected, anomaly.type)

    def test_analyze_invalid_tile(self):
        """"""
        analyze = analyzer.Analyzer()
        vals = [400 for n in range(255)]
        vals_2 = [300 for n in range(255)]

        dipole1 = Mock()
        dipole1.name = "dipole1"
        dipole1.values = json.dumps(vals)

        dipole2 = Mock()
        dipole2.name = "dipole2"
        dipole2.values = json.dumps(vals_2)

        observations = MagicMock()
        observations.__iter__.return_value = [dipole1, dipole2]

        expected = 12

        anomaly = analyze.analyze_invalid_tile(observations)

        self.assertEqual(expected, anomaly.type)

    def test_analyze_clustered_lines(self):
        """"""
        analyze = analyzer.Analyzer()
        vals = [400 for n in range(221)]
        vals_2 = [400 for n in range(221)]

        dipole1 = Mock()
        dipole1.name = "dipole1"
        dipole1.values = json.dumps(vals)

        all_off = Mock()
        all_off.name = "AllOff"
        all_off.values = json.dumps(vals_2)

        observations = MagicMock()
        observations.__iter__.return_value = [dipole1, all_off]

        expected = 3

        anomaly = analyze.analyze_clustered_lines(observations)

        self.assertEqual(expected, anomaly.type)

    def test_analyze_all_black_tile(self):
        """"""
        analyze = analyzer.Analyzer()
        vals = [0.9 for n in range(221)]
        vals_2 = [0.99 for n in range(221)]

        dipole1 = Mock()
        dipole1.name = "dipole1"
        dipole1.values = json.dumps(vals)

        dipole2 = Mock()
        dipole2.name = "dipole2"
        dipole2.values = json.dumps(vals_2)

        observations = MagicMock()
        observations.__iter__.return_value = [dipole1, dipole2]

        expected = 5

        anomaly = analyze.analyze_all_black_tile(observations)

        self.assertEqual(expected, anomaly.type)

    def test_low_powered_tile(self):
        """"""
        analyze = analyzer.Analyzer()
        vals = [2 for n in range(221)]
        vals_2 = [4 for n in range(221)]

        dipole1 = Mock()
        dipole1.name = "dipole1"
        dipole1.values = json.dumps(vals)

        dipole2 = Mock()
        dipole2.name = "dipole2"
        dipole2.values = json.dumps(vals_2)

        observations = MagicMock()
        observations.__iter__.return_value = [dipole1, dipole2]

        expected = 7

        anomaly = analyze.analyze_low_powered_tile(observations)

        self.assertEqual(expected, anomaly.type)

    def test_analyze_all_off(self):
        """"""
        analyze = analyzer.Analyzer()
        vals = [20 for n in range(221)]

        all_off = Mock()
        all_off.name = "AllOff"
        all_off.values = json.dumps(vals)

        observations = MagicMock()
        observations.__iter__.return_value = [all_off]

        expected = 2
        anomaly = analyze.analyze_all_off(observations)

        self.assertEqual(expected, anomaly.type)

    def test_analyze_black_dipole(self):
        """"""
        analyze = analyzer.Analyzer()

        vals = [8 for n in range(221)]
        vals_2 = [8 for n in range(221)]
        vals_3 = [8 for n in range(221)]
        vals_4 = [8 for n in range(221)]
        vals_5 = [8 for n in range(221)]
        vals_6 = [8 for n in range(221)]
        vals_7 = [8 for n in range(221)]
        vals_8 = [8 for n in range(221)]
        vals_9 = [8 for n in range(221)]
        vals_10 = [8 for n in range(221)]
        vals_11 = [8 for n in range(221)]
        vals_12 = [8 for n in range(221)]
        vals_13 = [8 for n in range(221)]
        vals_14 = [8 for n in range(221)]
        vals_15 = [8 for n in range(221)]
        anon_vals = [0.8 for n in range(221)]

        dipole1 = Mock()
        dipole1.name = "dipole1"
        dipole1.values = json.dumps(vals)

        dipole2 = Mock()
        dipole2.name = "dipole2"
        dipole2.values = json.dumps(vals_2)

        dipole3 = Mock()
        dipole3.name = "dipole3"
        dipole3.values = json.dumps(vals_3)

        dipole4 = Mock()
        dipole4.name = "dipole4"
        dipole4.values = json.dumps(vals_4)

        dipole5 = Mock()
        dipole5.name = "dipole5"
        dipole5.values = json.dumps(vals_5)

        dipole6 = Mock()
        dipole6.name = "dipole6"
        dipole6.values = json.dumps(vals_6)

        dipole7 = Mock()
        dipole7.name = "dipole7"
        dipole7.values = json.dumps(vals_7)

        dipole8 = Mock()
        dipole8.name = "dipole8"
        dipole8.values = json.dumps(vals_8)

        dipole9 = Mock()
        dipole9.name = "dipole9"
        dipole9.values = json.dumps(vals_9)

        dipole10 = Mock()
        dipole10.name = "dipole10"
        dipole10.values = json.dumps(vals_10)

        dipole11 = Mock()
        dipole11.name = "dipole11"
        dipole11.values = json.dumps(vals_11)

        dipole12 = Mock()
        dipole12.name = "dipole12"
        dipole12.values = json.dumps(vals_12)

        dipole13 = Mock()
        dipole13.name = "dipole13"
        dipole13.values = json.dumps(vals_13)

        dipole14 = Mock()
        dipole14.name = "dipole14"
        dipole14.values = json.dumps(vals_14)

        dipole15 = Mock()
        dipole15.name = "dipole15"
        dipole15.values = json.dumps(vals_15)

        anon_dip = Mock()
        anon_dip.name = "anon_dipole"
        anon_dip.values = json.dumps(anon_vals)

        observations = MagicMock()
        observations.__iter__.return_value = [dipole1, dipole2, dipole3, dipole4, dipole5,
                                              dipole6, dipole7, dipole8, dipole9, dipole10,
                                              dipole11, dipole12, dipole13, dipole14, dipole15, anon_dip]

        expected = 6

        anomalies = analyze.analyze_black_dipole(observations)
        for anon in anomalies:
            self.assertEqual(expected, anon.type)

    def test_analyze_rf_pollution(self):
        """"""
        analyze = analyzer.Analyzer()
        vals = [20 for n in range(221)]

        all_off = Mock()
        all_off.name = "AllOff"
        all_off.values = json.dumps(vals)

        dipole1 = Mock()
        dipole1.name = "dipole1"
        dipole1.values = json.dumps(vals)

        dipole2 = Mock()
        dipole2.name = "dipole2"
        dipole2.values = json.dumps(vals)

        observations = MagicMock()
        observations.__iter__.return_value = [dipole1, dipole2, all_off]

        expected = 8

        anomaly = analyze.analyze_rf_pollution(observations)

        self.assertEqual(expected, anomaly.type)

    def test_lower_power_historically(self):
        """"""
        analyze = analyzer.Analyzer()

        vals = [8 for n in range(172)]
        vals_2 = [8 for n in range(172)]
        vals_3 = [8 for n in range(172)]
        vals_4 = [8 for n in range(172)]
        vals_5 = [8 for n in range(172)]
        vals_6 = [8 for n in range(172)]
        vals_7 = [8 for n in range(172)]
        vals_8 = [8 for n in range(172)]
        vals_9 = [8 for n in range(172)]
        vals_10 = [8 for n in range(172)]
        vals_11 = [8 for n in range(172)]
        vals_12 = [8 for n in range(172)]
        vals_13 = [8 for n in range(172)]
        vals_14 = [8 for n in range(172)]
        vals_15 = [8 for n in range(172)]
        anon_vals = [1 for n in range(172)]

        dipole1 = Mock()
        dipole1.name = "dipole1"
        dipole1.values = json.dumps(vals)

        dipole2 = Mock()
        dipole2.name = "dipole2"
        dipole2.values = json.dumps(vals_2)

        dipole3 = Mock()
        dipole3.name = "dipole3"
        dipole3.values = json.dumps(vals_3)

        dipole4 = Mock()
        dipole4.name = "dipole4"
        dipole4.values = json.dumps(vals_4)

        dipole5 = Mock()
        dipole5.name = "dipole5"
        dipole5.values = json.dumps(vals_5)

        dipole6 = Mock()
        dipole6.name = "dipole6"
        dipole6.values = json.dumps(vals_6)

        dipole7 = Mock()
        dipole7.name = "dipole7"
        dipole7.values = json.dumps(vals_7)

        dipole8 = Mock()
        dipole8.name = "dipole8"
        dipole8.values = json.dumps(vals_8)

        dipole9 = Mock()
        dipole9.name = "dipole9"
        dipole9.values = json.dumps(vals_9)

        dipole10 = Mock()
        dipole10.name = "dipole10"
        dipole10.values = json.dumps(vals_10)

        dipole11 = Mock()
        dipole11.name = "dipole11"
        dipole11.values = json.dumps(vals_11)

        dipole12 = Mock()
        dipole12.name = "dipole12"
        dipole12.values = json.dumps(vals_12)

        dipole13 = Mock()
        dipole13.name = "dipole13"
        dipole13.values = json.dumps(vals_13)

        dipole14 = Mock()
        dipole14.name = "dipole14"
        dipole14.values = json.dumps(vals_14)

        dipole15 = Mock()
        dipole15.name = "dipole15"
        dipole15.values = json.dumps(vals_15)

        anon_dip = Mock()
        anon_dip.name = "anon_dipole"
        anon_dip.values = json.dumps(anon_vals)

        observations = MagicMock()
        observations.__iter__.return_value = [dipole1, dipole2, dipole3, dipole4, dipole5,
                                              dipole6, dipole7, dipole8, dipole9, dipole10,
                                              dipole11, dipole12, dipole13, dipole14, dipole15, anon_dip]

        expected = 9

        anomalies = analyze.analyze_lower_power(observations)
        for anon in anomalies:
            self.assertEqual(expected, anon.type)

    def test_higher_power_historically(self):
        """"""
        analyze = analyzer.Analyzer()

        vals = [8 for n in range(221)]
        vals_2 = [8 for n in range(221)]
        vals_3 = [8 for n in range(221)]
        vals_4 = [8 for n in range(221)]
        vals_5 = [8 for n in range(221)]
        vals_6 = [8 for n in range(221)]
        vals_7 = [8 for n in range(221)]
        vals_8 = [8 for n in range(221)]
        vals_9 = [8 for n in range(221)]
        vals_10 = [8 for n in range(221)]
        vals_11 = [8 for n in range(221)]
        vals_12 = [8 for n in range(221)]
        vals_13 = [8 for n in range(221)]
        vals_14 = [8 for n in range(221)]
        vals_15 = [8 for n in range(221)]
        anon_vals = [20 for n in range(221)]

        dipole1 = Mock()
        dipole1.name = "dipole1"
        dipole1.values = json.dumps(vals)

        dipole2 = Mock()
        dipole2.name = "dipole2"
        dipole2.values = json.dumps(vals_2)

        dipole3 = Mock()
        dipole3.name = "dipole3"
        dipole3.values = json.dumps(vals_3)

        dipole4 = Mock()
        dipole4.name = "dipole4"
        dipole4.values = json.dumps(vals_4)

        dipole5 = Mock()
        dipole5.name = "dipole5"
        dipole5.values = json.dumps(vals_5)

        dipole6 = Mock()
        dipole6.name = "dipole6"
        dipole6.values = json.dumps(vals_6)

        dipole7 = Mock()
        dipole7.name = "dipole7"
        dipole7.values = json.dumps(vals_7)

        dipole8 = Mock()
        dipole8.name = "dipole8"
        dipole8.values = json.dumps(vals_8)

        dipole9 = Mock()
        dipole9.name = "dipole9"
        dipole9.values = json.dumps(vals_9)

        dipole10 = Mock()
        dipole10.name = "dipole10"
        dipole10.values = json.dumps(vals_10)

        dipole11 = Mock()
        dipole11.name = "dipole11"
        dipole11.values = json.dumps(vals_11)

        dipole12 = Mock()
        dipole12.name = "dipole12"
        dipole12.values = json.dumps(vals_12)

        dipole13 = Mock()
        dipole13.name = "dipole13"
        dipole13.values = json.dumps(vals_13)

        dipole14 = Mock()
        dipole14.name = "dipole14"
        dipole14.values = json.dumps(vals_14)

        dipole15 = Mock()
        dipole15.name = "dipole15"
        dipole15.values = json.dumps(vals_15)

        anon_dip = Mock()
        anon_dip.name = "anon_dipole"
        anon_dip.values = json.dumps(anon_vals)

        observations = MagicMock()
        observations.__iter__.return_value = [dipole1, dipole2, dipole3, dipole4, dipole5,
                                              dipole6, dipole7, dipole8, dipole9, dipole10,
                                              dipole11, dipole12, dipole13, dipole14, dipole15, anon_dip]

        expected = 9

        anomalies = analyze.analyze_higher_power(observations)
        for anon in anomalies:
            self.assertEqual(expected, anon.type)

    def test_dipole_drifts_historically(self):
        """"""
        analyze = analyzer.Analyzer()

        vals = [8 for n in range(172)]
        vals_2 = [8 for n in range(172)]
        vals_3 = [8 for n in range(172)]
        vals_4 = [8 for n in range(172)]
        vals_5 = [8 for n in range(172)]
        vals_6 = [8 for n in range(172)]
        vals_7 = [8 for n in range(172)]
        vals_8 = [8 for n in range(172)]
        vals_9 = [8 for n in range(172)]
        vals_10 = [8 for n in range(172)]
        vals_11 = [8 for n in range(172)]
        vals_12 = [8 for n in range(172)]
        vals_13 = [8 for n in range(172)]
        vals_14 = [8 for n in range(172)]
        vals_15 = [8 for n in range(172)]
        anon_vals = [11 for n in range(172)]

        dipole1 = Mock()
        dipole1.name = "dipole1"
        dipole1.values = json.dumps(vals)

        dipole2 = Mock()
        dipole2.name = "dipole2"
        dipole2.values = json.dumps(vals_2)

        dipole3 = Mock()
        dipole3.name = "dipole3"
        dipole3.values = json.dumps(vals_3)

        dipole4 = Mock()
        dipole4.name = "dipole4"
        dipole4.values = json.dumps(vals_4)

        dipole5 = Mock()
        dipole5.name = "dipole5"
        dipole5.values = json.dumps(vals_5)

        dipole6 = Mock()
        dipole6.name = "dipole6"
        dipole6.values = json.dumps(vals_6)

        dipole7 = Mock()
        dipole7.name = "dipole7"
        dipole7.values = json.dumps(vals_7)

        dipole8 = Mock()
        dipole8.name = "dipole8"
        dipole8.values = json.dumps(vals_8)

        dipole9 = Mock()
        dipole9.name = "dipole9"
        dipole9.values = json.dumps(vals_9)

        dipole10 = Mock()
        dipole10.name = "dipole10"
        dipole10.values = json.dumps(vals_10)

        dipole11 = Mock()
        dipole11.name = "dipole11"
        dipole11.values = json.dumps(vals_11)

        dipole12 = Mock()
        dipole12.name = "dipole12"
        dipole12.values = json.dumps(vals_12)

        dipole13 = Mock()
        dipole13.name = "dipole13"
        dipole13.values = json.dumps(vals_13)

        dipole14 = Mock()
        dipole14.name = "dipole14"
        dipole14.values = json.dumps(vals_14)

        dipole15 = Mock()
        dipole15.name = "dipole15"
        dipole15.values = json.dumps(vals_15)

        anon_dip = Mock()
        anon_dip.name = "anon_dipole"
        anon_dip.values = json.dumps(anon_vals)

        observations = MagicMock()
        observations.__iter__.return_value = [dipole1, dipole2, dipole3, dipole4, dipole5,
                                              dipole6, dipole7, dipole8, dipole9, dipole10,
                                              dipole11, dipole12, dipole13, dipole14, dipole15, anon_dip]

        expected = 10

        anomalies = analyze.analyze_dipole_drifts(observations)
        for anon in anomalies:
            self.assertEqual(expected, anon.type)

    def test_analyze_peak_stronger_on_x_than_y(self):
        """"""
        analyze = analyzer.Analyzer()
        x_vals = [25 for n in range(220)]
        y_vals = [20 for n in range(220)]
        x_vals2 = [10 for n in range(220)]
        y_vals2 = [15 for n in range(220)]

        x_dip = Mock()
        x_dip.name = "dipole1_x"
        x_dip.values = json.dumps(x_vals)

        x_dip2 = Mock()
        x_dip2.name = "dipole2_x"
        x_dip2.values = json.dumps(x_vals2)

        y_dip = Mock()
        y_dip.name = "dipole1_y"
        y_dip.values = json.dumps(y_vals)

        y_dip2 = Mock()
        y_dip2.name = "dipole2_y"
        y_dip2.values = json.dumps(y_vals2)

        x_observations = MagicMock()
        x_observations.__iter__.return_value = [x_dip, x_dip2]
        x_observations.__len__.return_value = 2

        y_observations = MagicMock()
        y_observations.__iter__.return_value = [y_dip, y_dip2]
        y_observations.__len__.return_value = 2

        expected = 4
        anomalies = analyze.analyze_peak_stronger_on_x_than_y(x_observations, y_observations)
        for anon in anomalies:
            self.assertEqual(expected, anon.type)

    def test_fix_noise(self):
        """"""
        analyze = analyzer.Analyzer()
        mock_test = MagicMock()
        vals = [1 for n in range(100)]
        average = [1000 for n in range(12)]
        vals = vals + average

        observat = Mock()
        observat.values = json.dumps(vals)

        observations = MagicMock()
        observations.__iter__.return_value = [observat]
        mock_test.observations = observations

        expected = [1 for n in range(112)]

        new_test = analyze.fix_noise(mock_test)

        for new_obs in new_test.observations:
            self.assertTrue(expected, new_obs.values)
