import unittest

from app import create_app
from database.model import AnomalyType
from test.setup_test import database_set_up, database_tear_down
from view_model.anomaly_type_view import AnomalyTypeView


class AnomalyTypeViewTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the anomaly_type_view file.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def init_anomaly_type(self):
        """
        Tests that the default constructor can accept an AnomalyType.
        """
        session = self.app.session_factory()

        anomaly_type = session.query(AnomalyType).first()
        session.close()

        anomaly_type_view = AnomalyTypeView(anomaly_type)

        self.assertEquals(anomaly_type_view.id, anomaly_type.id)
        self.assertEquals(anomaly_type_view.name, anomaly_type.name)
        self.assertEquals(anomaly_type_view.description, anomaly_type_view.description)

    def init_dict(self):
        """
        Tests that the default constructor can accept the dictionary of an AnomalyType.
        """
        session = self.app.session_factory()

        anomaly_type = session.query(AnomalyType).first()
        session.close()

        good_data = {
            "id": anomaly_type.id,
            "name": anomaly_type.name,
            "description": anomaly_type.description
        }

        anomaly_type_view = AnomalyTypeView(good_data)

        self.assertEquals(anomaly_type_view.id, good_data["id"])
        self.assertEquals(anomaly_type_view.name, good_data["name"])
        self.assertEquals(anomaly_type_view.description, good_data["description"])

    def init_none(self):
        """
        Tests that the default constructor can set default values for an AnomalyType.
        """
        anomaly_type_view = AnomalyTypeView(None, None)

        self.assertEquals(anomaly_type_view.id, -1)
        self.assertEquals(anomaly_type_view.name, "default_anomaly_type")
        self.assertEquals(anomaly_type_view.description, "no desc")
