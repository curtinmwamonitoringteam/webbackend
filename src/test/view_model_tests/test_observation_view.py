import unittest

from app import create_app
from database.model import Observation
from test.setup_test import database_set_up, database_tear_down
from view_model.observation_view import ObservationView


class ObservationViewTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the observation_view file.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def init_anomaly_type(self):
        """
        Tests that the default constructor can accept an Observation.
        """
        session = self.app.session_factory()

        observation = session.query(Observation).first()
        session.close()

        observation_view = ObservationView(observation)

        self.assertEquals(observation_view.id, observation.id)
        self.assertEquals(observation_view.name, observation.name)
        self.assertEquals(observation_view.start_time, observation.start_time)
        self.assertEquals(observation_view.dipole_id, observation.dipole_id)
        self.assertEquals(observation_view.values, observation.values)

    def init_dict(self):
        """
        Tests that the default constructor can accept the dictionary of an Observation.
        """
        session = self.app.session_factory()

        observation = session.query(Observation).first()
        session.close()

        good_data = {
            "id": observation.id,
            "name": observation.name,
            "start_time": observation.start_time,
            "dipole_id": observation.dipole_id,
            "values": observation.values
        }

        observation_view = ObservationView(good_data)

        self.assertEquals(observation_view.id, good_data["id"])
        self.assertEquals(observation_view.name, good_data["name"])
        self.assertEquals(observation_view.start_time, good_data["start_time"])
        self.assertEquals(observation_view.dipole_id, good_data["dipole_id"])
        self.assertEquals(observation_view.values, good_data["values"])

    def init_none(self):
        """
        Tests that the default constructor can set default values for an Observation.
        """
        dipole_view = ObservationView(None, None)

        self.assertEquals(dipole_view.id, -1)
        self.assertEquals(dipole_view.name, "default_obs")
        self.assertEquals(dipole_view.start_time, -1)
        self.assertEquals(dipole_view.dipole_id, -1)
        self.assertEquals(dipole_view.values, [])
