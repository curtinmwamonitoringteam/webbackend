import unittest

from app import create_app
from database.model import Dipole
from test.setup_test import database_set_up, database_tear_down
from view_model.dipole_view import DipoleView


class DipoleViewTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the dipole_view file.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def init_anomaly_type(self):
        """
        Tests that the default constructor can accept a Dipole.
        """
        session = self.app.session_factory()

        dipole = session.query(Dipole).first()
        session.close()

        dipole_view = DipoleView(dipole)

        self.assertEquals(dipole_view.id, dipole.id)
        self.assertEquals(dipole_view.tile_id, dipole.tile_id)
        self.assertEquals(dipole_view.dipole_number, dipole.dipole_number)
        self.assertEquals(dipole_view.polarity, dipole.polarity)

    def init_dict(self):
        """
        Tests that the default constructor can accept the dictionary of a Dipole.
        """
        session = self.app.session_factory()

        dipole = session.query(Dipole).first()
        session.close()

        good_data = {
            "id": dipole.id,
            "tile_id": dipole.tile_id,
            "dipole_number": dipole.dipole_number,
            "polarity": dipole.polarity
        }

        dipole_view = DipoleView(good_data)

        self.assertEquals(dipole_view.id, good_data["id"])
        self.assertEquals(dipole_view.tile_id, good_data["tile_id"])
        self.assertEquals(dipole_view.dipole_number, good_data["dipole_number"])
        self.assertEquals(dipole_view.polarity, good_data["polarity"])

    def init_none(self):
        """
        Tests that the default constructor can set default values for a Dipole.
        """
        dipole_view = DipoleView(None, None)

        self.assertEquals(dipole_view.id, -1)
        self.assertEquals(dipole_view.tile_id, -1)
        self.assertEquals(dipole_view.dipole_number, -1)
        self.assertEquals(dipole_view.polarity, -1)
