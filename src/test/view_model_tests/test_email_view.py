import unittest

from app import create_app
from database.model import EmailInformation
from test.setup_test import database_set_up, database_tear_down
from view_model.email_view import EmailView


class EmailViewTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the email_view file.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def init_email(self):
        """
        Tests that the default construction can accept an Email Information.
        """
        session = self.app.session_factory()

        email = session.query(EmailInformation).first()
        session.close()

        email_view = EmailView(email)

        self.assertEquals(email_view.id, email.id)
        self.assertEquals(email_view.email_address, email.email_address)

    def init_dict(self):
        """
        Tests that the default constructor can accept the dictionary of an Email Information.
        """
        session = self.app.session_factory()

        email = session.query(EmailInformation).first()
        session.close()

        good_data = {
            "id": email.id,
            "email_address": email.email_address
        }

        email_view = EmailView(good_data)

        self.assertEquals(email_view.id, email.id)
        self.assertEquals(email_view.email_address, email.email_address)

    def init_none(self):
        """
        Tests that the default constructor can set default values for an Email Information.
        """
        email_view = EmailView(None, None)

        self.assertEquals(email_view.id, -1)
        self.assertEquals(email_view.email_address, '')
