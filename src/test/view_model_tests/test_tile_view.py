import unittest

from app import create_app
from database.model import Tile
from test.setup_test import database_set_up, database_tear_down
from view_model.tile_view import TileView


class TileViewTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the observation_view file.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def init_anomaly_type(self):
        """
        Tests that the default constructor can accept a Tile.
        """
        session = self.app.session_factory()

        test = session.query(Tile).first()
        session.close()

        test_view = TileView(test)

        self.assertEquals(test_view.id, test.id)
        self.assertEquals(test_view.name, test.name)

    def init_dict(self):
        """
        Tests that the default constructor can accept the dictionary of a Tile.
        """
        session = self.app.session_factory()

        test = session.query(Tile).first()
        session.close()

        good_data = {
            "id": test.id,
            "name": test.name
        }

        test_view = TileView(good_data)

        self.assertEquals(test_view.id, good_data["id"])
        self.assertEquals(test_view.name, good_data["name"])

    def init_none(self):
        """
        Tests that the default constructor can set default values for a Tile.
        """
        test_view = TileView(None, None)

        self.assertEquals(test_view.id, -1)
        self.assertEquals(test_view.name, "")
