import unittest

from app import create_app
from database.model import IgnoreTile
from test.setup_test import database_set_up, database_tear_down
from view_model.ignore_tile_view import IgnoreTileView


class IgnoreTileViewTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the ignore_tile_view file.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def init_anomaly_type(self):
        """
        Tests that the default constructor can accept a IgnoreTile.
        """
        session = self.app.session_factory()

        ignore_tile = session.query(IgnoreTile).first()
        session.close()

        ignore_tile_view = IgnoreTileView(ignore_tile)

        self.assertEquals(ignore_tile_view.id, ignore_tile.id)
        self.assertEquals(ignore_tile_view.tile_id, ignore_tile.tile_id)

    def init_dict(self):
        """
        Tests that the default constructor can accept the dictionary of a IgnoreTile.
        """
        session = self.app.session_factory()

        ignore_tile = session.query(IgnoreTile).first()
        session.close()

        good_data = {
            "id": ignore_tile.id,
            "tile_id": ignore_tile.tile_id
        }

        ignore_tile_view = IgnoreTileView(good_data)

        self.assertEquals(ignore_tile_view.id, good_data["id"])
        self.assertEquals(ignore_tile_view.tile_id, good_data["tile_id"])

    def init_none(self):
        """
        Tests that the default constructor can set default values for a IgnoreTile.
        """
        ignore_tile_view = IgnoreTileView(None, None)

        self.assertEquals(ignore_tile_view.id, -1)
        self.assertEquals(ignore_tile_view.tile_id, -1)
