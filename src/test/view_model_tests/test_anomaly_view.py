import unittest

from app import create_app
from database.model import Anomaly
from test.setup_test import database_set_up, database_tear_down
from view_model.anomaly_view import AnomalyView


class AnomalyViewTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the anomaly_view file.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def init_anomaly_type(self):
        """
        Tests that the default constructor can accept an Anomaly.
        """
        session = self.app.session_factory()

        anomaly = session.query(Anomaly).first()
        session.close()

        anomaly_view = AnomalyView(anomaly)

        self.assertEquals(anomaly_view.id, anomaly.id)
        self.assertEquals(anomaly_view.type, anomaly.type)
        self.assertEquals(anomaly_view.observation, anomaly_view.observation)

    def init_dict(self):
        """
        Tests that the default constructor can accept the dictionary of an Anomaly.
        """
        session = self.app.session_factory()

        anomaly = session.query(Anomaly).first()
        session.close()

        good_data = {
            "id": anomaly.id,
            "type": anomaly.type,
            "observation": anomaly.observation
        }

        anomaly_view = AnomalyView(good_data)

        self.assertEquals(anomaly_view.id, good_data["id"])
        self.assertEquals(anomaly_view.type, good_data["type"])
        self.assertEquals(anomaly_view.observation, good_data["observation"])

    def init_none(self):
        """
        Tests that the default constructor can set default values for an Anomaly.
        """
        anomaly_view = AnomalyView(None, None)

        self.assertEquals(anomaly_view.id, -1)
        self.assertEquals(anomaly_view.type, -1)
        self.assertEquals(anomaly_view.observation, -1)
