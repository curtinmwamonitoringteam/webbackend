import unittest

from app import create_app
from database.model import Test
from test.setup_test import database_set_up, database_tear_down
from view_model.test_view import TestView


class TestViewTestCase(unittest.TestCase):
    """
    Class that performs unit tests on the observation_view file.
    """

    def setUp(self):
        """
        Overrides method in TestCase.

        Logic for setting up the application for unit testing.
        """
        self.app = create_app()
        self.client = self.app.test_client()
        database_set_up(self.app)

    def tearDown(self):
        """
        Overrides method in TestCase.

        Logic for tearing down (cleaning up) the application after unit testing.
        """
        database_tear_down(self.app)

    def init_anomaly_type(self):
        """
        Tests that the default constructor can accept a Test.
        """
        session = self.app.session_factory()

        test = session.query(Test).first()
        session.close()

        test_view = TestView(test)

        self.assertEquals(test_view.id, test.id)
        self.assertEquals(test_view.tile_id, test.tile_id)
        self.assertEquals(test_view.start_time, test.start_time)
        self.assertEquals(test_view.observations, test.observations)

    def init_dict(self):
        """
        Tests that the default constructor can accept the dictionary of a Test.
        """
        session = self.app.session_factory()

        test = session.query(Test).first()
        session.close()

        good_data = {
            "id": test.id,
            "tile_id": test.tile_id,
            "start_time": test.start_time,
            "observation": test.observations
        }

        test_view = TestView(good_data)

        self.assertEquals(test_view.id, good_data["id"])
        self.assertEquals(test_view.tile_id, good_data["tile_id"])
        self.assertEquals(test_view.start_time, good_data["start_time"])
        self.assertEquals(test_view.observations, good_data["observation"])

    def init_none(self):
        """
        Tests that the default constructor can set default values for a Test.
        """
        test_view = TestView(None, None)

        self.assertEquals(test_view.id, -1)
        self.assertEquals(test_view.tile_id, -1)
        self.assertEquals(test_view.start_time, -1)
        self.assertEquals(test_view.observations, [])
