import copy

from database.model import Tile
from repository import session_factory
from view_model.tile_view import TileView


def get_all():
    """
    Get all Tiles.

    Returns:
        ret (list): A list of all tiles.
    """
    session = session_factory()
    data = []
    query = session.query(Tile)

    for tile in query:
        data.append(tile)

    ret = copy.deepcopy(data)

    session.close()

    return ret


def get_by_id(tile_id):
    """
    Get a single Tile that has a matching ID.

    Args:
        tile_id (int): ID of a tile.

    Returns:
        ret (Tile): A single tile.

    Raises:
        TypeError
    """
    if not isinstance(tile_id, (int, long)):
        raise TypeError('Tile ID must be an integer')

    session = session_factory()

    tile = session.query(Tile) \
        .filter(Tile.id == tile_id) \
        .first()

    ret = copy.deepcopy(tile)

    session.close()

    return ret


def create(data):
    """
    Create a tile.

    Args:
        data (Tile): Represents a tile.

    Raises:
        TypeError
    """
    if not isinstance(data, (Tile, TileView)):
        raise TypeError('Must be of type Tile or TileView')

    session = session_factory()

    session.add(data)
    session.commit()

    session.close()


def update(data):
    """
    Update a tile.

    Args:
        data (Tile): Represents a tile with updated data.

    Raises:
        TypeError
    """
    if not isinstance(data, (Tile, TileView)):
        raise TypeError('Must be of type Tile or TileView')

    session = session_factory()

    tile = session.query(Tile) \
        .filter(Tile.id == data.id) \
        .first()

    tile.name = data.name
    session.commit()

    session.close()


def delete_by_id(tile_id):
    """
    Delete a tile given an ID.

    Args:
        tile_id (int): ID of a tile.

    Raises:
        TypeError
    """
    if not isinstance(tile_id, (int, long)):
        raise TypeError('Tile ID must be an integer')

    session = session_factory()
    session.query(Tile) \
        .filter(Tile.id == tile_id) \
        .delete()

    session.commit()
    session.close()
