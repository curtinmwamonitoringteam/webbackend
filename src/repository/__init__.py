import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app import app_config

# DB connection engine
engine = create_engine(app_config[os.getenv('APP_SETTINGS')].DATABASE_URI)

# Create session factory
session_factory = sessionmaker(bind=engine, autoflush=True)
