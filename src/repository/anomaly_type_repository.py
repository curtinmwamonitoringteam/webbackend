import copy

from database.model import AnomalyType
from repository import session_factory
from view_model.anomaly_type_view import AnomalyTypeView


def get_all():
    """
    Get all AnomalyTypes.

    Returns:
        ret (list): A list containing all anomaly types.
    """
    session = session_factory()
    data = []
    query = session.query(AnomalyType)

    for anom_type in query:
        data.append(anom_type)

    ret = copy.deepcopy(data)

    session.close()

    return ret


def get_by_id(type_id):
    """
    Get a single AnomalyType that has a matching ID.

    Args:
        type_id (int): ID of an anomaly type to find.

    Returns:
        ret (AnomalyType): An anomaly type associated with the given type_id.

    Raises:
        TypeError
    """
    if not isinstance(type_id, (int, long)):
        raise TypeError('AnomalyType ID must be an integer')

    session = session_factory()

    anom_type = session.query(AnomalyType) \
        .filter(AnomalyType.id == type_id) \
        .first()

    ret = copy.deepcopy(anom_type)

    session.close()

    return ret


def create(data):
    """
    Create an AnomalyType, returns associated row id.

    Args:
        data (AnomalyType or AnomalyTypeView): Data of an anomaly type to create.

    Returns:
        type_id (int): ID of the associated row created.

    Raises:
        TypeError
    """
    if not isinstance(data, (AnomalyType, AnomalyTypeView)):
        raise TypeError('Must be of type AnomalyType or AnomalyTypeView')

    session = session_factory()

    session.add(data)
    session.commit()

    type_id = copy.deepcopy(data.id)

    session.close()

    return type_id


def update(data):
    """
    Update an anomaly type given an AnomalyType object.

    Args:
        data (AnomalyType or AnomalyTypeView): Data of an anomaly type to update.

    Raises:
        TypeError
    """
    if not isinstance(data, (AnomalyType, AnomalyTypeView)):
        raise TypeError('Must be of type AnomalyType or AnomalyTypeView')

    session = session_factory()

    anom_type = session.query(AnomalyType) \
        .filter(AnomalyType.id == data.id) \
        .first()

    anom_type.name = data.name
    anom_type.description = data.description
    session.commit()

    session.close()


def delete_by_id(type_id):
    """
    Delete an anomaly type given an id.

    Args:
        type_id (int): ID of an anomaly type to delete.

    Raises:
        TypeError
    """
    if not isinstance(type_id, (int, long)):
        raise TypeError('AnomalyType ID must be an integer')

    session = session_factory()

    session.query(AnomalyType) \
        .filter(AnomalyType.id == type_id) \
        .delete()

    session.commit()
    session.close()


def get_by_id_list(type_id_list):
    """
    Get all AnomalyTypes with matching IDs from the given list.
    """
    session = session_factory()
    search = [x.strip() for x in type_id_list.split(',')]

    query = session.query(AnomalyType) \
        .filter(AnomalyType.id.in_(search))

    types = []

    for anom_type in query:
        types.append(anom_type)

    ret = copy.deepcopy(types)

    session.close()

    return ret
