import copy

from database.model import EmailInformation
from repository import session_factory
from view_model.email_view import EmailView


def get_all():
    """
    Get all Email Information.

    Returns:
        ret (list): A list of all email information
    """
    session = session_factory()
    data = []

    query = session.query(EmailInformation)

    for email in query:
        data.append(email)

    ret = copy.deepcopy(data)

    session.close()

    return ret


def get_by_email(email_address):
    """
    Get a single Email Information that has a matching email address.

    Args:
        email_address (string): Email Address of an email information.

    Raises:
        TypeError
    """
    if not isinstance(email_address, (str, basestring)):
        raise TypeError('Email Address must be a string')

    session = session_factory()

    email = session.query(EmailInformation) \
        .filter(EmailInformation.email_address == email_address) \
        .first()

    ret = copy.deepcopy(email)

    session.close()

    return ret


def create(data):
    """
    Create an Email Information, returns associated row ID

    Args:
        data (EmailInformation): Represents an email information.

    Returns:
        id (int): Email Information row ID.

    Raises:
        TypeError
    """
    if not isinstance(data, (EmailInformation, EmailView)):
        raise TypeError('Data must be of type EmailInformation or EmailView')

    session = session_factory()

    query = session.query(EmailInformation) \
        .filter(EmailInformation.email_address == data.email_address) \
        .first()

    if query is None:
        session.add(data)
        session.commit()

        email_id = copy.deepcopy(data.id)
        session.close()

        return email_id
    else:
        return 0


def delete_by_email(email_address):
    """
    Delete an email information given an email address.

    Args:
        email_address (string): Email Address of an email information.

    Raises:
        TypeError
    """
    if not isinstance(email_address, (str, basestring)):
        raise TypeError('Email Address must be a string')

    session = session_factory()

    session.query(EmailInformation) \
        .filter(EmailInformation.email_address == email_address) \
        .delete()

    session.commit()
    session.close()
