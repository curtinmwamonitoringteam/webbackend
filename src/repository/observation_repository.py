import copy

from database.model import Observation
from repository import session_factory
from view_model.observation_view import ObservationView


def get_all():
    """
    Get all Observations.

    Returns:
        ret (list): A list of all observations.
    """
    session = session_factory()
    data = []
    query = session.query(Observation)

    for observation in query:
        data.append(observation)

    ret = copy.deepcopy(data)

    session.close()

    return ret


def get_by_id(observation_id):
    """
    Get a single Observation that has a matching ID.

    Args:
        observation_id (int): ID of an observation to find.

    Returns:
        ret (Observation): A single observation.

    Raises:
        TypeError
    """
    if not isinstance(observation_id, (int, long)):
        raise TypeError('Observation ID must be an integer')

    session = session_factory()

    observation = session.query(Observation) \
        .filter(Observation.id == observation_id) \
        .first()

    ret = copy.deepcopy(observation)

    session.close()

    return ret


def create(data):
    """
    Create an Observation, returns associated row ID.

    Args:
        data (Observation): Represents an observation.

    Returns:
        obs_id (int): Observation row ID.

    Raises:
        TypeError
    """
    if not isinstance(data, (Observation, ObservationView)):
        raise TypeError('Must be of type Observation or ObservationView')

    session = session_factory()

    session.add(data)
    session.commit()

    obs_id = copy.deepcopy(data.id)

    session.close()

    return obs_id


def update(data):
    """
    Update an observation.

    Args:
        data (Observation): Represents an observation with updated data.

    Raises:
        TypeError
    """
    if not isinstance(data, (Observation, ObservationView)):
        raise TypeError('Must be of type Observation or ObservationView')

    session = session_factory()

    observation = session.query(Observation) \
        .filter(Observation.id == data.id) \
        .first()

    observation.name = data.name
    observation.start_time = data.start_time
    observation.dipole_id = data.dipole_id
    observation.values = data.values

    session.commit()

    session.close()


def delete_by_id(observation_id):
    """
    Delete an observation given an ID.

    Args:
        observation_id (int); ID of an observation.

    Raises:
        TypeError
    """
    if not isinstance(observation_id, (int, long)):
        raise TypeError('Observation ID must be an integer')

    session = session_factory()
    session.query(Observation) \
        .filter(Observation.id == observation_id) \
        .delete()

    session.commit()
    session.close()
