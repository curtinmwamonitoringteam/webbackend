import copy

from database.model import Anomaly
from repository import session_factory
from view_model.anomaly_view import AnomalyView


def get_all():
    """
    Get all Anomalies.

    Returns:
        ret (list): A list of all anomalies.
    """
    session = session_factory()
    data = []
    query = session.query(Anomaly)

    for anomaly in query:
        data.append(anomaly)

    ret = copy.deepcopy(data)

    session.close()

    return ret


def get_by_id(anom_id):
    """
    Get a single Anomaly that has a matching ID.

    Args:
        anom_id (int): ID of an anomaly to fetch.

    Returns:
        ret (Anomaly): A single anomaly.

    Raises:
        TypeError
    """
    if not isinstance(anom_id, (int, long)):
        raise TypeError('Anomaly ID must be an integer')

    session = session_factory()

    anomaly = session.query(Anomaly) \
        .filter(Anomaly.id == anom_id) \
        .first()

    ret = copy.deepcopy(anomaly)

    session.close()

    return ret


def create(data):
    """
    Create an Anomaly, returns associated row id.

    Args:
        data (Anomaly): Represents an anomaly.

    Returns:
        anom_id (int): Anomaly row ID.

    Raises:
        TypeError
    """
    if not isinstance(data, (Anomaly, AnomalyView)):
        raise TypeError('Must be of type Anomaly or AnomalyView')

    session = session_factory()

    session.add(data)
    session.commit()

    anom_id = copy.deepcopy(data.id)

    session.close()

    return anom_id


def delete_by_id(anom_id):
    """
    Delete an anomaly given an id.

    Args:
        anom_id (int): ID of an anomaly to delete.

    Raises:
        TypeError
    """
    if not isinstance(anom_id, (int, long)):
        raise TypeError('Anomaly ID must be an integer')

    session = session_factory()
    session.query(Anomaly) \
        .filter(Anomaly.id == anom_id) \
        .delete()

    session.commit()
    session.close()


def get_by_obs_id_list(obs_id_list):
    """
    Get anomalies associated with specific observations.

    Args:
        obs_id_list (str): A comma separated string containing observation ID's to find.

    Returns:
        ret (list): A list of associated anomalies.
    """
    session = session_factory()
    search = [x.strip() for x in obs_id_list.split(',')]

    query = session.query(Anomaly) \
        .filter(Anomaly.observation.in_(search))

    anomalies = []

    for anom in query:
        anomalies.append(anom)

    ret = copy.deepcopy(anomalies)

    session.close()

    return ret
