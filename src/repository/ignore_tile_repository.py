import copy

from database.model import IgnoreTile
from repository import session_factory
from view_model.ignore_tile_view import IgnoreTileView


def get_all():
    """
    Get all IgnoredTiles.

    Returns:
        ret (list): A list containing all ignored tiles in the database.
    """
    session = session_factory()
    data = []

    query = session.query(IgnoreTile)

    for ignore in query:
        data.append(ignore)

    ret = copy.deepcopy(data)

    session.close()

    return ret


def create(data):
    """
    Create an IgnoreTile, returns associated row ID.

    Args:
        data (IgnoreTile): Data of a IgnoreTile to create.

    Returns:
         ignore_id (int): ID of the associated row created.

    Raises:
        TypeError
    """
    if not isinstance(data, (IgnoreTile, IgnoreTileView)):
        raise TypeError('Must be of type IgnoreTile or IgnoreTileView')

    session = session_factory()

    session.add(data)
    session.commit()

    ignore_id = copy.deepcopy(data.id)

    session.close()

    return ignore_id


def delete_by_id(ignore_id):
    """
    Delete an IgnoreTile given an ID.

    Args:
        ignore_id (int): ID of an IgnoreTile to delete.

    Raises:
        TypeError
    """
    if not isinstance(ignore_id, (int, long)):
        raise TypeError('Ignore ID must be an integer')

    session = session_factory()

    session.query(IgnoreTile) \
        .filter(IgnoreTile.id == ignore_id) \
        .delete()

    session.commit()
    session.close()
