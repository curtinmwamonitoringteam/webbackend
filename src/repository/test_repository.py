import copy

from sqlalchemy.orm import joinedload

from database.model import Test
from repository import session_factory
from view_model.test_view import TestView


def get_all(get_obs=False):
    """
    Get all Tests.

    Args:
        get_obs (bool): Pass as true to eager load observations.

    Returns:
        ret (list): A list of all tests.
    """
    session = session_factory()
    data = []

    if get_obs == 'true':
        # Eager load observations
        query = session.query(Test) \
            .options(joinedload(Test.observations))
    else:
        query = session.query(Test)

    for test in query:
        data.append(test)

    ret = copy.deepcopy(data)

    session.close()

    return ret


def get_by_id(test_id, get_obs=False):
    """
    Get a single Test that has a matching ID.

    Args:
        test_id (int): ID of a test.
        get_obs (bool): Pass as true to eager load observations.

    Raises:
        TypeError
    """
    if not isinstance(test_id, (int, long)):
        raise TypeError('Test ID must be an integer')

    session = session_factory()

    if get_obs == 'true':
        # Eager load observations
        test = session.query(Test) \
            .options(joinedload(Test.observations)) \
            .filter(Test.id == test_id) \
            .first()
    else:
        test = session.query(Test) \
            .filter(Test.id == test_id) \
            .first()

    ret = copy.deepcopy(test)

    session.close()

    return ret


def create(data):
    """
    Create a Test, returns associated row ID.

    Args:
        data (Test): Represents a test.

    Returns:
        test_id (int): Test row ID.

    Raises:
        TypeError
    """
    if not isinstance(data, (Test, TestView)):
        raise TypeError('Data must be of type Test or TestView')

    session = session_factory()

    session.add(data)
    session.commit()

    test_id = copy.deepcopy(data.id)

    session.close()

    return test_id


def create_all(data):
    """
    Create all Tests in a list.

    Args:
        data (list): A list of tests.
    """
    session = session_factory()

    session.add_all(data)

    session.flush()
    session.close()


def update(data):
    """
    Update a test. Does not update observations.

    Args:
        data (Test): Represents a test with updated data.

    Raises:
        TypeError
    """
    if not isinstance(data, (Test, TestView)):
        raise TypeError('Data must be of type Test or TestView')

    session = session_factory()

    test = session.query(Test) \
        .filter(Test.id == data.id) \
        .first()

    test.tile_id = data.tile_id
    test.start_time = data.start_time

    session.commit()
    session.close()


def delete_by_id(test_id):
    """
    Delete a test given an ID.

    Args:
        test_id (int): ID of a test.

    Raises:
        TypeError
    """
    if not isinstance(test_id, (int, long)):
        raise TypeError('Test ID must be an integer')

    session = session_factory()

    session.query(Test) \
        .filter(Test.id == test_id) \
        .delete()

    session.commit()
    session.close()


def get_by_start_time(start_time, get_obs = False):
    """
    Get all tests with start_time == time.

    Args:
        start_time (int): The tests starting time.
        get_obs (bool): If true then return the observations associated with the test.

    Returns:
        ret (dict): A tuple of tests.

    Raises:
        TypeError
    """
    print("start")

    if not isinstance(start_time, (int, long)):
        raise TypeError('Start time must be an integer')

    session = session_factory()
    data = []
    query = []

    if get_obs == True:
        # Eager load observations
        query = session.query(Test) \
            .options(joinedload(Test.observations)) \
            .filter(Test.start_time == start_time)
    else:
        print("here")
        query = session.query(Test) \
            .filter(Test.start_time == start_time)
        print("no here")

    for test in query:
        data.append(test)

    ret = copy.deepcopy(data)

    session.close()

    return ret


def get_by_start_time_range(start_time, end_time, get_obs = False):
    """
    Get all tests with Test.start_time and Test.end_time in range.

    Args:
        start_time (int): The tests starting time.
        end_time (int): The tests ending time.
        get_obs (bool): If true then return the observations associated with the tests.

    Returns:
        ret (list): All tests within a time range.

    Raises:
        TypeError
    """
    if not isinstance(start_time, (int, long)):
        raise TypeError('Start time must be an integer')

    if not isinstance(end_time, (int, long)):
        raise TypeError('End time must be an integer')

    session = session_factory()
    data = []

    if get_obs == 'true':
        # eager load observations
        query = session.query(Test) \
            .options(joinedload(Test.observations)) \
            .filter(Test.start_time >= start_time, Test.start_time <= end_time)
    else:
        query = session.query(Test) \
            .filter(Test.start_time >= start_time, Test.start_time <= end_time)

    for test in query:
        data.append(test)

    ret = copy.deepcopy(data)

    session.close()

    return ret


def get_by_id_range(start_id, end_id, get_obs):
    """
    Get all tests with Test.start_id and Test.end_id in range.

    Args:
        start_id (int): The tests starting id.
        end_id (int): The tests ending id.
        get_obs (bool): If true then return the observations associated with the tests.

    Returns:
        ret (list): All tests within an id range.

    Raises:
        TypeError
    """
    if not isinstance(start_id, (int, long)):
        raise TypeError('Start id must be an integer')

    if not isinstance(end_id, (int, long)):
        raise TypeError('End id must be an integer')

    session = session_factory()
    data = []

    if get_obs == 'true':
        # eager load observations
        query = session.query(Test) \
            .options(joinedload(Test.observations)) \
            .filter(Test.id >= start_id, Test.id <= end_id)
    else:
        query = session.query(Test) \
            .filter(Test.id >= start_id, Test.id <= end_id)

    for test in query:
        data.append(test)

    ret = copy.deepcopy(data)

    session.close()

    return ret