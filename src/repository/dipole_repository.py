import copy

from database.model import Dipole
from repository import session_factory
from view_model.dipole_view import DipoleView


def get_all():
    """
    Get all Dipoles.

    Returns:
        ret (list): A list containing all dipoles in the database.
    """
    session = session_factory()
    data = []
    query = session.query(Dipole)

    for dipole in query:
        data.append(dipole)

    ret = copy.deepcopy(data)

    session.close()

    return ret


def get_by_id(dipole_id):
    """
    Get a single Dipole that has a matching ID.

    Args:
        dipole_id (int): ID of a dipole to find.

    Returns:
        ret (Dipole): A dipole matching the given ID.

    Raises:
        TypeError
    """
    if not isinstance(dipole_id, (int, long)):
        raise TypeError('AnomalyType ID must be an integer')

    session = session_factory()

    dipole = session.query(Dipole) \
        .filter(Dipole.id == dipole_id) \
        .first()

    ret = copy.deepcopy(dipole)

    session.close()

    return ret


def create(data):
    """
    Create a Dipole, returns associated row id.

    Args:
        data (Dipole): Data of a Dipole to create.

    Returns:
        dipole_id (int): ID of the associated row created.

    Raises:
        TypeError
    """
    if not isinstance(data, (Dipole, DipoleView)):
        raise TypeError('Must be of type Dipole or DipoleView')

    session = session_factory()

    session.add(data)
    session.commit()

    dipole_id = copy.deepcopy(data.id)

    session.close()

    return dipole_id


def update(data):
    """
    Update a dipole.

    Args:
        data (Dipole): Data of a dipole to update.

    Raises:
        TypeError
    """
    if not isinstance(data, (Dipole, DipoleView)):
        raise TypeError('Must be of type Dipole or DipoleView')

    session = session_factory()

    dipole = session.query(Dipole) \
        .filter(Dipole.id == data.id) \
        .first()

    dipole.tile_id = data.tile_id
    dipole.dipole_number = data.dipole_number
    dipole.polarity = data.polarity

    session.commit()

    session.close()


def delete_by_id(dipole_id):
    """
    Delete a dipole given an ID.

    Args:
        dipole_id (int): ID of a dipole to delete.

    Raises:
        TypeError
    """
    if not isinstance(dipole_id, (int, long)):
        raise TypeError('Dipole ID must be an integer')

    session = session_factory()

    session.query(Dipole) \
        .filter(Dipole.id == dipole_id) \
        .delete()

    session.commit()
    session.close()


def get_dipole_id(tile_id, dipole_number, polarity):
    """
    Get Dipole ID with fields matching params.

    Args:
        tile_id (int): ID of a tile.
        dipole_number (int): Number of an observation to search within.
        polarity (int): Polarity of a dipole.::

            0 -- X
            1 -- Y

    Returns:
        ret (int): ID of a matching dipole.

    Raises:
        TypeError, ValueError
    """
    if not isinstance(tile_id, (int, long)):
        raise TypeError('Tile ID must be an integer')

    if not isinstance(dipole_number, (int, long)):
        raise TypeError('Observation number must be an integer')

    if not isinstance(polarity, (int, long)):
        raise TypeError('Polarity must be an integer')

    if polarity < 0 or polarity > 1:
        raise ValueError('Polarity must be either 0 or 1')

    session = session_factory()

    dipole = session.query(Dipole) \
        .filter(Dipole.tile_id == tile_id) \
        .filter(Dipole.dipole_number == dipole_number) \
        .filter(Dipole.polarity == polarity) \
        .first()

    ret = None

    if dipole is not None:
        ret = copy.deepcopy(dipole.id)

    session.close()

    return ret


def get_by_tile_id(tile_id):
    """
    Get all Dipoles with a matching tile ID.

    Args:
        tile_id (int): ID of a tile.

    Returns:
        ret (list): A list of dipoles with matching tile ID's

    Raises:
        TypeError
    """
    if not isinstance(tile_id, (int, long)):
        raise TypeError('Tile ID must be an integer')

    session = session_factory()

    query = session.query(Dipole) \
        .filter(Dipole.tile_id == tile_id)

    dipoles = []

    for dipole in query:
        dipoles.append(dipole)

    ret = copy.deepcopy(dipoles)

    session.close()

    return ret
