from app import create_app
from app.endpoint.anomaly_endpoint import anomaly_endpoints
from app.endpoint.anomaly_type_endpoint import anomalyType_endpoints
from app.endpoint.dipole_endpoint import dipole_endpoints
from app.endpoint.email_endpoint import email_endpoints
from app.endpoint.ignore_tile_endpoint import ignore_tile_endpoints
from app.endpoint.observation_endpoint import observation_endpoints
from app.endpoint.test_endpoint import test_endpoints
from app.endpoint.tile_endpoint import tile_endpoints
from app.endpoint.upload_endpoint import upload_endpoints
from app.exceptions import error_handlers

# App setup
app = create_app()

# Register endpoints
app.register_blueprint(anomalyType_endpoints)
app.register_blueprint(anomaly_endpoints)
app.register_blueprint(dipole_endpoints)
app.register_blueprint(email_endpoints)
app.register_blueprint(ignore_tile_endpoints)
app.register_blueprint(observation_endpoints)
app.register_blueprint(test_endpoints)
app.register_blueprint(tile_endpoints)
app.register_blueprint(upload_endpoints)

# Register exceptions
app.register_blueprint(error_handlers)


# Default endpoint
@app.route('/')
def landing():
    """
    Serves as a default endpoint for the application.

    Returns:
        (html): Default HTML to be displayed to the user.
    """
    return '<h1>MWA Monitoring Service</h1>' \
           'The API reference for this service can be found ' \
           '<a href="https://bitbucket.org/curtinmwamonitoringteam/webbackend/wiki/api_reference">' \
           'here</a>'


if __name__ == '__main__':
    app.run()
