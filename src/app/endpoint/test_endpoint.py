from flask import Blueprint, request

from service.test_service import *

test_endpoints = Blueprint('test_endpoints', __name__)


@test_endpoints.route('/test', methods=['GET', 'POST'])
def test():
    """
    Serves GET / POST requests for test's.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.::

            GET  -- Get all test's.
            POST -- Post a single anomaly.
    """
    if request.method == 'GET':
        # Get params
        start_time = request.args.get('start_time', default=None)
        end_time = request.args.get('end_time', default=None)
        start_id = request.args.get('start_id', default=None)
        end_id = request.args.get('end_id', default=None)
        get_obs = request.args.get('get_obs', default=False)

        if end_time is not None and start_time is not None:
            return get_tests_in_range(int(start_time), int(end_time), get_obs)
        elif start_id is not None and end_id is not None:
            return get_tests_in_id_range(int(start_id), int(end_id), get_obs)
        elif start_time is not None:
            return get_test_by_start_time(int(start_time), get_obs)
        else:
            return get_test_all(get_obs)

    elif request.method == 'POST':
        return post_test(request)


@test_endpoints.route('/test/<int:test_id>', methods=['GET', 'PUT', 'DELETE'])
def test_id(test_id):
    """
    Serves GET / PUT / DELETE requests for a given test ID.

    Args:
        test_id (int): ID of a test type.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.::

            GET    -- Get test by it's ID.
            PUT    -- Put a test by it's ID.
            DELETE -- Delete a test by it's ID.
    """
    if request.method == 'GET':
        return get_test_by_id(test_id, get_obs=request.args.get('get_obs', default=False))
    elif request.method == 'PUT':
        return put_test_by_id(request, test_id)
    elif request.method == 'DELETE':
        return delete_test_by_id(test_id)
