from flask import Blueprint, request

from service.ignore_tile_service import get_ignore_tile_all, post_ignore_tile, delete_ignore_tile

ignore_tile_endpoints = Blueprint('ignore_tile_endpoints ', __name__)


@ignore_tile_endpoints.route('/ignore_tile', methods=['GET', 'POST'])
def ignore_tile():
    """
    Serves GET / POST requests for ignored tiles.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.::

            GET  -- Get all ignored tiles.
            POST -- Post a single ignored tile.
    """
    if request.method == 'GET':
        return get_ignore_tile_all()
    elif request.method == 'POST':
        return post_ignore_tile(request)


@ignore_tile_endpoints.route('/ignore_tile/<int:ignore_id>', methods=['DELETE'])
def ignore_tile_id(ignore_id):
    """
    Serves DELETE requests for a given ignored tiled ID.

    Args:
        ignore_id (int): ID of an ignored tile.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.::

            DELETE -- Delete an ignored tile by it's ID.
    """
    if request.method == 'DELETE':
        return delete_ignore_tile(ignore_id)
