from flask import Blueprint, request

from service.dipole_service import *

dipole_endpoints = Blueprint('dipole_endpoints', __name__)


@dipole_endpoints.route('/dipole', methods=['GET'])
def dipole():
    """
    Serves GET requests for dipole's.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.::

            GET -- Get dipole's by it's tile ID.
            GET -- Get all dipole's.
    """
    if request.method == 'GET':
        tile_id = request.args.get('tile_id', default=None)

        if tile_id is not None:
            return get_dipoles_by_tile_id(int(tile_id))
        else:
            return get_dipole_all()


@dipole_endpoints.route('/dipole/<int:dipole_id>', methods=['GET'])
def dipole_id(dipole_id):
    """
    Serves GET requests for a given dipole ID.

    Args:
        dipole_id (int): ID of a dipole.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.::

            GET -- Get a dipole by it's ID.
    """
    if request.method == 'GET':
        return get_dipole_by_id(dipole_id)
