from flask import Blueprint, request

from service.upload_service import upload_file

upload_endpoints = Blueprint('upload_endpoints', __name__)


@upload_endpoints.route('/upload', methods=['POST'])
def upload():
    """
    Serves POST requests for upload.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.::

            POST -- Upload a given file.
    """
    return upload_file(request)
