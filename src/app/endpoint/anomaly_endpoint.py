from flask import Blueprint, request

from service.anomaly_service import *

anomaly_endpoints = Blueprint('anomaly_endpoints', __name__)


@anomaly_endpoints.route('/anomaly', methods=['GET', 'POST'])
def anomaly():
    """
    Serves GET / POST requests for anomalies.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.::

            GET  -- Get an anomaly by observation ID.
            GET  -- Get all anomalies (if observation_id_list is empty).
            POST -- Post a single anomaly.
    """
    if request.method == 'GET':
        obs_id_list = request.args.get('obs_id_list', default=None)

        if obs_id_list is not None:
            return get_anomaly_by_obs_id(obs_id_list)
        else:
            return get_anomaly_all()
    elif request.method == 'POST':
        return post_anomaly(request)


@anomaly_endpoints.route('/anomaly/<int:anom_id>', methods=['GET', 'PUT', 'DELETE'])
def anomaly_id(anom_id):
    """
    Serves GET / PUT / DELETE requests for a given anomaly ID.

    Args:
        anom_id (int): ID of an anomaly.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.::

            GET    -- Get the anomaly by it's ID.
            DELETE -- Delete an anomaly by it's ID.
    """
    if request.method == 'GET':
        return get_anomaly_by_id(anom_id)
    elif request.method == 'DELETE':
        return delete_anomaly_by_id(anom_id)
