from flask import Blueprint, request

from service.email_service import *

email_endpoints = Blueprint('email_endpoints', __name__)

@email_endpoints.route('/email', methods=['GET', 'POST'])
def email_information():
    """
    Serves GET / POST / PUT requests for email information

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.::

            GET -- Get all email information
            POST -- Post a single email information
    """
    if request.method == 'GET':
        return get_email_all()
    elif request.method == 'POST':
        return post_email(request)


@email_endpoints.route('/email/<string:email_address>', methods=['GET', 'DELETE'])
def email_information_address(email_address):
    """
    Serves GET / DELETE requests for email information

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.::

            GET -- Get an email information by its email address.
            DELETE -- Delete an email information by its email address.
    """
    if request.method == 'GET':
        return get_by_email(email_address)
    elif request.method == 'DELETE':
        return delete_by_email_address(email_address)