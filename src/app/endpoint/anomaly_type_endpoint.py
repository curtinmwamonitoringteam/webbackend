from flask import Blueprint, request

from service.anomaly_type_service import *

anomalyType_endpoints = Blueprint('anomalyType_endpoints', __name__)


@anomalyType_endpoints.route('/anomaly-type', methods=['GET', 'POST'])
def anomaly_type():
    """
    Serves GET / POST requests for anomaly types.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.::

            GET  -- Get all anomaly types.
            POST -- Post a single anomaly.
    """
    if request.method == 'GET':
        type_id_list = request.args.get('type_id_list', default=None)

        if type_id_list is not None:
            return get_anomaly_type_by_id_list(type_id_list)
        else:
            return get_anomaly_type_all()
    elif request.method == 'POST':
        return post_anomaly_type(request)


@anomalyType_endpoints.route('/anomaly-type/<int:type_id>', methods=['GET', 'PUT', 'DELETE'])
def anomaly_type_id(type_id):
    """
    Serves GET / PUT / DELETE requests for a given anomaly type ID.

    Args:
        type_id (int): ID of an anomaly type.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.::

            GET    -- Get the anomaly type by it's ID.
            PUT    -- Put an anomaly type by it's ID.
            DELETE -- Delete an anomaly type by it's ID.
    """
    if request.method == 'GET':
        return get_anomaly_type_by_id(type_id)
    elif request.method == 'PUT':
        return put_anomaly_type_by_id(request, type_id)
    elif request.method == 'DELETE':
        return delete_anomaly_type_by_id(type_id)
