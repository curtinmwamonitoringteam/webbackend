from flask import Blueprint, request

from service.tile_service import get_tile_all, get_tile_by_id

tile_endpoints = Blueprint('tile_endpoints', __name__)


@tile_endpoints.route('/tile', methods=['GET'])
def tile():
    """
    Serves GET requests for tile's.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.::

            GET -- Get all tile's.
    """
    if request.method == 'GET':
        return get_tile_all()


@tile_endpoints.route('/tile/<int:tile_id>', methods=['GET'])
def tile_id(tile_id):
    """
    Serves GET requests for a given tile's ID.

    Args:
        tile_id (int): ID of a tile.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.::

            GET -- Get a tile by it's ID.
    """
    if request.method == 'GET':
        return get_tile_by_id(tile_id)
