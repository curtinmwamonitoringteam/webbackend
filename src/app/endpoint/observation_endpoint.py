from flask import Blueprint, request

from service.observation_service import *

observation_endpoints = Blueprint('observation_endpoints', __name__)


@observation_endpoints.route('/observation', methods=['GET', 'POST'])
def observation():
    """
    Serves GET / POST requests for anomalies.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.::

            GET  -- Get an anomaly by observation ID.
            GET  -- Get all anomalies (if observation_id_list is empty).
            POST -- Post a single anomaly.
    """
    if request.method == 'GET':
        return get_observation_all()
    elif request.method == 'POST':
        return post_observation(request)


@observation_endpoints.route('/observation/<int:obs_id>', methods=['GET', 'PUT', 'DELETE'])
def observation_id(obs_id):
    """
    Serves GET / PUT / DELETE requests for a given observation ID.

    Args:
        obs_id (int): ID of an observation.

    Returns:
        resp (Response): Resulting response (status code, data, etc.) based on the request type.::

            GET    -- Get the observation by it's ID.
            PUT    -- Put an observation by it's ID.
            DELETE -- Delete an observation by it's ID.
    """
    if request.method == 'GET':
        return get_observation_by_id(obs_id)
    elif request.method == 'PUT':
        return put_observation_by_id(request, obs_id)
    elif request.method == 'DELETE':
        return delete_observation_by_id(obs_id)
