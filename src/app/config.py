import os

if "APP_SETTINGS" not in os.environ:
    os.environ["APP_SETTINGS"] = "prod"


class Config(object):
    """
    Parent config class. Inherits from object.
    """
    DEBUG = False
    CSRF_ENABLED = True
    DATABASE_URI = 'mysql://mwauser:mwaicrar@localhost:3306/mwamonitor'
    UPLOAD_FOLDER = os.path.join(os.getcwd(), 'upload')
    ALLOWED_EXTENSIONS = {'gz'}
    MAX_CONTENT_LENGTH = 16 * 1024 * 1024  # 16MiB
    LOWER_POWER_SENSITIVITY = 3
    HIGHER_POWER_SENSITIVITY = 5
    RAINBOW_EFFECT_SENSITIVITY = 200
    DIPOLE_DRIFT_SENSITIVITY = 1

    #EMAIL SETTINGS
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USE_SSL = True
    MAIL_USERNAME = 'mwa.monitor@gmail.com'
    MAIL_PASSWORD = 'mwaadminuser'


class DevelopmentConfig(Config):
    """
    Dev config class. Inherits from the parent Config class.
    """
    DEBUG = True
    MAIL_SUPPRESS_SEND = True


class TestingConfig(Config):
    """
    Test config class, uses a separate database. Inherits from the parent Config class.
    """
    DEBUG = True
    TESTING = True
    DATABASE_URI = 'mysql://mwa_test_user:mwa_test_pwd@localhost:3306/mwa_test'
    UPLOAD_FOLDER = os.path.join(os.getcwd(), '../../upload')


class ProductionConfig(Config):
    """
    Production config class. Inherits from the parent Config class.
    """
    DEBUG = False
    TESTING = False


app_config = {
    'dev': DevelopmentConfig,
    'test': TestingConfig,
    'prod': ProductionConfig,
}
