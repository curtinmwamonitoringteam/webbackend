from flask import Response, Blueprint
from sqlalchemy.exc import OperationalError, IntegrityError, InvalidRequestError
from werkzeug.exceptions import BadRequestKeyError

error_handlers = Blueprint('error_handlers', __name__)


@error_handlers.app_errorhandler(OperationalError)
def handle_operational_error(error):
    """
    Web OperationalError handler. Wraps the error message into a Response object with the status code 500.

    Args:
        error (str): Error message for an OperationalError.

    Returns:
        resp (response): Response to be sent, containing the status code and data (error message).
    """
    resp = Response()
    resp.status_code = 500
    resp.data = 'OperationalError: {}'.format(error)

    return resp


@error_handlers.app_errorhandler(BadRequestKeyError)
def handle_bad_request_key_error(error):
    """
    Web BadRequestKeyError handler. Wraps the error message into a Response object with the status code 400.

    Args:
        error (str): Error message for an BadRequestKeyError.

    Returns:
        resp (response): Response to be sent, containing the status code and data (error message).
    """
    resp = Response()
    resp.status_code = 400
    resp.data = 'BadRequestKeyError: {}'.format(error)

    return resp


@error_handlers.app_errorhandler(IntegrityError)
def handle_integrity_error(error):
    """
    Web IntegrityError handler. Wraps the error message into a Response object with the status code 500.

    Args:
        error (str): Error message for an IntegrityError.

    Returns:
        resp (response): Response to be sent, containing the status code and data (error message).
    """
    resp = Response()
    resp.status_code = 500
    resp.data = 'IntegrityError: {}'.format(error)

    return resp


@error_handlers.app_errorhandler(InvalidRequestError)
def handle_invalid_request_error(error):
    """
    Web InvalidRequestError handler. Wraps the error message into a Response object with the status code 400.

    Args:
        error (str): Error message for an InvalidRequestError.

    Returns:
        resp (response): Response to be sent, containing the status code and data (error message).
    """
    resp = Response()
    resp.status_code = 400
    resp.data = 'InvalidRequestError: {}'.format(error)

    return resp
