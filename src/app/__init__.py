import os

from flask import Flask
from flask_cors import CORS
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app.config import app_config


def create_app():
    """
    Creates the flask app, sets up the config, database connection and session factory.

    Returns:
        app (obj): Represents an instance of this application.
    """
    app = Flask(__name__)
    CORS(app)

    # Set config
    app.config.from_object(app_config[os.getenv('APP_SETTINGS')])

    # Database connection engine
    app.engine = create_engine(app.config['DATABASE_URI'])

    # Create session factory
    app.session_factory = sessionmaker(bind=app.engine, autoflush=True)

    return app
