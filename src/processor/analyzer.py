import os
import json
import numpy as np
from database.model import Anomaly
from app import app_config


class Analyzer(object):
    """Class that analyzes a pickle file to find anomalies"""

    def analyze(self, test_list):
        """
        Calls all algorithms to analyze for anomalies.

        Args:
            test_list (list): A list of tests.

        Returns:
            anomaly (anomaly): A list of non empty anomalies for tests.
        """
        anomalies = []
        x_anomalies = []
        y_anomalies = []
        x_dipoles = []
        y_dipoles = []

        for test in test_list:
            # get observations with noise removed
            fixed = self.fix_noise(test)

            # Observations go x polarity then y polarity so step for each
            x_polarity = fixed.observations[0:len(fixed.observations):2]
            y_polarity = fixed.observations[1:len(fixed.observations):2]

            # If the high priority algorithms return anything for that tile(test) then the rest of the algorithms can be
            # skipped over as this means that the whole tile is anomalous.
            x_anomaly = self.analyze_high_priority_anomalies(x_polarity)

            if x_anomaly is not None:
                anomalies.append(x_anomaly)
                x_anomaly = None
            else:
                x_anomalies.append(self.analyze_all_off(x_polarity))
                x_anomalies.append(self.analyze_rf_pollution(x_polarity))

                # Can return a list of anomalies for each dipole
                x_dipoles.append(self.analyze_black_dipole(x_polarity))
                x_dipoles.append(self.analyze_lower_power(x_polarity))
                x_dipoles.append(self.analyze_higher_power(x_polarity))
                x_dipoles.append(self.analyze_peak_stronger_on_x_than_y(x_polarity, y_polarity))
                x_dipoles.append(self.analyze_dipole_zeroes(x_polarity))
                x_dipoles.append(self.analyze_dipole_drifts(x_polarity))

                # Flatten list and append to anomalies
                x_dipoles = sum(x_dipoles, [])
                for anomaly in x_dipoles:
                    if anomaly is not None:
                        x_anomalies.append(anomaly)
                        anomalies.append(anomaly)
                del x_dipoles[:]

            # Same as x_anomaly
            y_anomaly = self.analyze_high_priority_anomalies(y_polarity)

            if y_anomaly is not None:
                anomalies.append(y_anomaly)
                y_anomaly = None
            else:
                anomalies.append(self.analyze_all_off(y_polarity))
                anomalies.append(self.analyze_rf_pollution(y_polarity))

                y_dipoles.append(self.analyze_black_dipole(y_polarity))
                y_dipoles.append(self.analyze_lower_power(y_polarity))
                y_dipoles.append(self.analyze_higher_power(y_polarity))
                y_dipoles.append(self.analyze_dipole_zeroes(y_polarity))
                y_dipoles.append(self.analyze_dipole_drifts(y_polarity))

                y_dipoles = sum(y_dipoles, [])
                for anomaly in y_dipoles:
                    if anomaly is not None:
                        y_anomalies.append(anomaly)
                        anomalies.append(anomaly)
                del y_dipoles[:]

        # Remove None elements from list
        return [anomaly for anomaly in anomalies if anomaly is not None]

    def analyze_high_priority_anomalies(self, test):
        """
        Calls high priority algorithms to analyze for anomalies

        Args:
            test (test): A test for a specific tile.

        Returns:
            anomaly (anomaly): An anomaly objects representing discovered anomalies within a test.
        """
        anomaly = None

        # Treat functions as a list so we can check after each call if an anomaly has been returned
        list_functions = [self.analyze_invalid_tile, self.analyze_all_black_tile,
                          self.analyze_clustered_lines, self.analyze_rainbow_effect, self.analyze_low_powered_tile]

        for functions in list_functions:
            anomaly = functions(test)
            if anomaly is not None:
                return anomaly

        return anomaly

    def analyze_rainbow_effect(self, test):
        """
        Analyzes a test for evidence of a rainbow effect.

        Args:
            test (test): A test for a specific tile.

        Returns:
              anomaly (anomaly): An anomaly object representing discovered anomalies within a test
        """
        overall_values = []
        anomaly = None

        # Rainbow effect requires power in all on
        if self.analyze_all_off(test) is not None:
            for obs in test:
                if obs.name not in ("AllOff", "AllOn", "All_0"):
                    values = json.loads(obs.values)

                    # Only need to check the 'top' part of the curve - 101Mhz - 204Mhz
                    for freq in range(79, 160):
                        overall_values.append(values[freq])

            overall_average = np.mean(overall_values)

            # Get values from config file, default is 200
            if overall_average >= app_config[os.getenv('APP_SETTINGS')].RAINBOW_EFFECT_SENSITIVITY:
                anomaly = Anomaly()
                # We attach the AllOn dipole for tile anomalies
                anomaly.observation = test[0].id
                anomaly.type = 1

        return anomaly

    def analyze_invalid_tile(self, test):
        """
        Analyzes a test for evidence of an invalid tiles.

         Args:
            test (test): A test for a specific tile.

        Returns:
              anomaly (anomaly): An anomaly object representing discovered anomalies within a test
        """

        overall_values = []
        start_region_values = []
        anomaly = None

        for obs in test:
            if obs.name not in ("AllOn", "AllOff", "All_0"):
                values = json.loads(obs.values)
                for freq in range(1, 255):
                    overall_values.append(values[freq])

                    if freq < 48:
                        start_region_values.append(values[freq])

        overall_average = np.mean(overall_values)
        start_region_average = np.mean(start_region_values)

        if (overall_average > 300) and (start_region_average > 10):
            anomaly = Anomaly()
            anomaly.observation = test[0].id
            anomaly.type = 12

        return anomaly

    def analyze_clustered_lines(self, test):
        """
        Analyzes a test for the Clustered Lines anomaly. This is when the AllOff dipole will match every other
        dipole.

        Args:
            test (test): A test for a specific tile.

        Returns:
            anomaly (anomaly): An anomaly objects representing discovered anomalies within a test.
        """

        all_off_values = []
        dipole_values = []
        all_off_averages = []
        dipole_averages = []
        dipole_std = []
        anomaly = None

        # First we get the values for the AllOff dipole
        # Then we calculate the average for each ~25Mhz (20 Channels = 25.6Mhz)
        for obs in test:
            if obs.name in "AllOff":
                values = json.loads(obs.values)

                for freq in range(61, 221):
                    all_off_values.append(float(values[freq]))

                    if freq % 20 == 0:
                        all_off_averages.append(np.mean(all_off_values[len(all_off_values) -
                                                                       20:len(all_off_values) + 1]))
            elif obs.name not in ("AllOn", "AllOff", "All_0"):
                values = json.loads(obs.values)
                for freq in range(61, 221):
                    dipole_values.append(float(values[freq]))

                    if freq % 20 == 0:
                        dipole_averages.append((np.mean(dipole_values[len(dipole_values) - 20:len(dipole_values) + 1])))
                        dipole_std.append(np.std(dipole_values[len(dipole_values) - 20:len(dipole_values) + 1]))
        # We then get the overall averages and standard deviation for the rest of the dipoles(excluding AllOn and
        # AllOff).
        all_off_line = 0

        # Now we compare the dipole averages to the AllOff averages to see if the AllOff dipole is receiving the same
        #  amount of power as the rest of the dipoles. If so then it is the clustered lines anomaly. We add some leeway
        # to the limits as AllOff should be receiving no power so if it is anywhere close the the overall curve then
        # it is an anomaly.
        for val in range(0, 7):
            std_upper_limit = dipole_averages[val] + (dipole_std[val] * 2)
            std_lower_limit = dipole_averages[val] - (dipole_std[val] * 2)
            if (all_off_averages[val] <= std_upper_limit) and (all_off_averages[val] >= std_lower_limit):
                all_off_line += 1

        # We check over 125Mhz as we want to make sure it is following the line rather than the AllOff dipole receiving
        # power which is another type of anomaly.
        if all_off_line >= 5:
            anomaly = Anomaly()
            anomaly.observation = test[0].id
            anomaly.type = 3
        return anomaly

    def analyze_all_black_tile(self, test):
        """
        Analyze a test for the All Black Tile anomaly. This when all the dipoles are lower than a value
        of 5.

        Args:
            test (test): A test for a specific tile.

        Returns:
            anomaly (anomaly): An list of anomalies representing discovered anomalies within a test.
        """

        power_off_value = 2
        dipole_values = []
        anomaly = None

        # Get all dipole values and get the average of them. We examine the curve of the data which is 60-280Mhz which
        # is channel 47-220.
        for obs in test:
            values = json.loads(obs.values)
            for freq in range(47, 220):
                dipole_values.append(values[freq])

        dipoles_average = np.mean(dipole_values)
        # If the overall average falls below the value that is roughly normal(2) then the tile is black.
        if dipoles_average < power_off_value:
            anomaly = Anomaly()
            anomaly.observation = test[0].id
            anomaly.type = 5

        return anomaly

    def analyze_low_powered_tile(self, test):
        """
        Analyze a test for the lower powered anomaly. This is when the overall power of all dipoles is a lot lower
        than usual.

        Args:
            test (test): A test for a specific tile.

        Returns:
            anomaly (anomaly): An list of anomalies representing discovered anomalies within a test.
        """

        overall_values = []
        power_off_value = 1
        low_power_value = 5
        anomaly = None

        for obs in test:
            if obs.name not in ("AllOn", "AllOff", "All_0"):
                values = json.loads(obs.values)
                for freq in range(47, 220):
                    overall_values.append(values[freq])

        overall_average = np.mean(overall_values)

        if (overall_average > power_off_value) and (overall_average < low_power_value):
            anomaly = Anomaly()
            anomaly.observation = test[0].id
            anomaly.type = 7

        return anomaly

    def analyze_all_off(self, test):
        """
        Analyze a test for the Power while Off Anomaly. This is when the AllOff dipole is receiving power.

        Args:
            test (test): A test for a specific tile.

        Returns:
            anomaly (anomaly): An anomaly objects representing discovered anomalies within a test.
        """
        anomaly = None

        all_off_values = []
        for obs in test:
            if obs.name in "AllOff":
                values = json.loads(obs.values)
                for freq in range(47, 220):
                    all_off_values.append(values[freq])
        average = np.mean(all_off_values)
        if average > 2:
            # The All Off dipole should always contain no power at all
            anomaly = Anomaly()
            anomaly.observation = test[0].id
            anomaly.type = 2
        return anomaly

    def analyze_black_dipole(self, test):
        """
        Analyze a test for the Black Dipole anomaly. This is when a dipole is receiving minimal/no power.

        Args:
            test (test): A test for a specific tile.

        Returns:
            anomaly_list (list): An anomaly objects representing discovered anomalies within a test.
        """

        dipole_values = [[] for i in range(16)]
        overall_values = []
        dipole_num = 0
        anomaly_list = []

        for obs in test:
            if obs.name not in ("AllOff", "AllOn", "All_0"):
                values = json.loads(obs.values)
                for freq in range(47, 220):
                    overall_values.append(float(values[freq]))
                    dipole_values[dipole_num].append(values[freq])
                dipole_num += 1
        # Get the overall average for the tile
        overall_average = np.mean(overall_values)
        lower_limit = 1
        dipole_limit = 2
        no_zeroes = 0.5

        # Compare each dipoles average to the lower limit of 2. If it falls below it then the
        # dipole is dead. We also make sure there is no zeroes as that is a different anomaly.
        for idx, dipole in enumerate(dipole_values):
            average = np.mean(dipole)

            if (average < dipole_limit) and (int(overall_average) > lower_limit) and (average > no_zeroes):
                anomaly = Anomaly()
                anomaly.observation = test[idx + 2].id
                anomaly.type = 6
                anomaly_list.append(anomaly)

        return anomaly_list

    def analyze_rf_pollution(self, test):
        """
        Analyze a test for the RF Pollution Anomaly. This is when there is high power (purple) in the 0-60MHz range
        and while off for all dipoles.

        Args:
            test (test): A test for a specific tile.

        Returns:
            anomaly (anomaly): An anomaly objects representing discovered anomalies within a test.
        """
        power_off_value = 2
        dipole_values = []
        anomaly = None

        # RF Pollution contains power in the AllOff dipole.
        if self.analyze_all_off(test) is not None:
            for obs in test:
                if obs.name not in ("AllOff", "AllOn", "All_0"):
                    values = json.loads(obs.values)
                    # Each freq channel is 1.28Mhz so 47 = ~60Mhz.
                    for freq in range(1, 47):
                        dipole_values.append(float(values[freq]))

            dipole_average = np.mean(dipole_values)

            # The values in the first 60Mhz of a dipole are generally around 0.80Mhz to 0.99Mhz. So if its higher than
            # that on average, we can conclude there is RF Pollution. We will give some lee way however.
            if dipole_average > power_off_value:
                anomaly = Anomaly()
                anomaly.observation = test[0].id
                anomaly.type = 8

        return anomaly

    def analyze_lower_power(self, test):
        """
        Analyze a test for the lower power historically anomaly. This is when a dipole is below than the standard
        deviation since last fixed.

        Args:
            test (test): A test for a specific tile.

        Returns:
            anomaly (anomaly): An anomaly objects representing discovered anomalies within a test.
        """

        dipole_values = [[] for i in range(16)]
        dipole_averages = []
        overall_vals = [[] for i in range(11)]
        dipole_anomalous = [[] for i in range(16)]
        overall_average = []
        overall_std = []
        dipole_num = 0
        count = 0
        mhz_range = 10
        anomalies = []

        # First we calculate the average for each ~12Mhz (10 Channels = 12Mhz) for each dipole
        for obs in test:
            if obs.name not in ("AllOn", "AllOff", "All_0"):
                values = json.loads(obs.values)
                for freq in range(61, 172):
                    dipole_values[dipole_num].append(values[freq])
                    if freq % mhz_range == 0:
                        average = np.mean(dipole_values[dipole_num][len(dipole_values[dipole_num]) - mhz_range:len(
                            dipole_values[dipole_num]) + 1])
                        dipole_averages.append(round(average))
                dipole_num += 1
                # dipole_num is used here as a test has a length of 20 but we only want 16 dipoles from it.

        # Now we get the values for each 12Mhz over all dipoles
        for dipole in dipole_values:
            if dipole:
                for vals in range(mhz_range, len(dipole)):
                    if vals % mhz_range == 0:
                        overall_vals[count].append(dipole[vals - mhz_range: vals + 1])
                        count += 1
            count = 0

        # Now we get the average and standard deviation for each 12Mhz over all dipoles.
        for vals in range(len(overall_vals)):
            overall_average.append(np.mean(overall_vals[vals]))
            overall_std.append(np.std(overall_vals[vals]))

        # 110/10 = 11. So that we means we have 11 averages for each 10Mhz. For each dipole get its average, the
        # overall average and the standard deviation for all dipoles. We then subtract the std from the average as well
        # as a bit more which gives more or less sensitivity. Compare this to the dipole average and if it is under
        # then label that 10mhz as anomalous.
        dipole_num = 0
        for val in range(0, len(dipole_averages), 11):
            chunk = dipole_averages[val:val + 11]
            for v in range(0, mhz_range):
                std_lower_limit = round(overall_average[v] - overall_std[v] -
                                        app_config[os.getenv('APP_SETTINGS')].LOWER_POWER_SENSITIVITY)
                if chunk[v] < std_lower_limit:
                    dipole_anomalous[dipole_num].append(True)
                else:
                    dipole_anomalous[dipole_num].append(False)
            dipole_num += 1

        # We then see if there is 2 True values in a row to see if it is anomalous over 25Mhz. This gives added
        # accuracy. We then use mutual recursion to get the history for each dipole that might be anomalous.
        # If it was anomalous before then flag it.
        for idx, dipole in enumerate(dipole_anomalous):
            dipole_anomaly = False
            for val1, val2 in zip(dipole, dipole[1:]):
                if val1 and val2 and not dipole_anomaly:
                    anomaly = Anomaly()
                    anomaly.observation = test[idx + 2].id
                    anomaly.type = 9
                    anomalies.append(anomaly)

                    dipole_anomaly = True

        return anomalies

    def analyze_higher_power(self, test):
        """
        Analyze a test for the lower power historically anomaly. This is when a dipole is below than the standard
        deviation since last fixed.

        Args:
            test (test): A test for a specific tile.

        Returns:
            anomaly (anomaly): An anomaly objects representing discovered anomalies within a test.
        """

        dipole_values = [[] for i in range(16)]
        dipole_averages = []
        overall_vals = [[] for i in range(11)]
        dipole_anomalous = [[] for i in range(16)]
        overall_average = []
        overall_std = []
        dipole_num = 0
        count = 0
        mhz_range = 10
        std_limit_sensitivity = 5
        anomalies = []

        # First we calculate the average for each ~12Mhz (10 Channels = 12Mhz) for each dipole
        for obs in test:
            if obs.name not in ("AllOn", "AllOff", "All_0"):
                values = json.loads(obs.values)
                for freq in range(61, 172):
                    dipole_values[dipole_num].append(values[freq])
                    if freq % mhz_range == 0:
                        average = np.mean(dipole_values[dipole_num][len(dipole_values[dipole_num]) - mhz_range:len(
                            dipole_values[dipole_num]) + 1])
                        dipole_averages.append(round(average))
                dipole_num += 1
                # dipole_num is used here as a test has a length of 20 but we only want 16 dipoles from it.

        # Now we get the values for each 12Mhz over all dipoles
        for dipole in dipole_values:
            if dipole:
                for vals in range(mhz_range, len(dipole)):
                    if vals % mhz_range == 0:
                        overall_vals[count].append(dipole[vals - mhz_range: vals + 1])
                        count += 1
            count = 0

        # Now we get the average and standard deviation for each 12Mhz over all dipoles.
        for vals in range(len(overall_vals)):
            overall_average.append(np.mean(overall_vals[vals]))
            overall_std.append(np.std(overall_vals[vals]))

        # 110/10 = 11. So that we means we have 11 averages for each 10Mhz. For each dipole get its average, the
        # overall average and the standard deviation for all dipoles. We then subtract the std from the average as well
        # as a bit more which gives more or less sensitivity. Compare this to the dipole average and if it is under
        # then label that 10mhz as anomalous.
        dipole_num = 0
        for val in range(0, len(dipole_averages), 11):
            chunk = dipole_averages[val:val + 11]
            for v in range(0, mhz_range):
                std_upper_limit = round(overall_average[v] + overall_std[v] +
                                        app_config[os.getenv('APP_SETTINGS')].HIGHER_POWER_SENSITIVITY)
                if chunk[v] > std_upper_limit:
                    dipole_anomalous[dipole_num].append(True)
                else:
                    dipole_anomalous[dipole_num].append(False)
            dipole_num += 1

        # We then see if there is 2 True values in a row to see if it is anomalous over 25Mhz. This gives added
        # accuracy. We then use mutual recursion to get the history for each dipole that might be anomalous.
        # If it was anomalous before then flag it.
        for idx, dipole in enumerate(dipole_anomalous):
            dipole_anomaly = False
            for val1, val2 in zip(dipole, dipole[1:]):
                if val1 and val2 and not dipole_anomaly:
                    anomaly = Anomaly()
                    anomaly.observation = test[idx + 2].id
                    anomaly.type = 9
                    anomalies.append(anomaly)

                    dipole_anomaly = True

        return anomalies

    def analyze_dipole_drifts(self, test):
        """
        Analyze a test for the dipole drifts anomaly. This is when a dipole is below than the standard
        deviation.

        Args:
            test (test): A test for a specific tile.

        Returns:
            anomaly (anomaly): A list of anomalies
        """

        dipole_values = [[] for i in range(16)]
        dipole_averages = []
        overall_vals = [[] for i in range(11)]
        dipole_anomalous = [[] for i in range(16)]
        overall_average = []
        overall_std = []
        dipole_num = 0
        count = 0
        mhz_range = 10
        std_limit_sensitivity = 5
        anomalies = []

        # First we calculate the average for each ~12Mhz (10 Channels = 12Mhz) for each dipole
        for obs in test:
            if obs.name not in ("AllOn", "AllOff", "All_0"):
                values = json.loads(obs.values)
                for freq in range(61, 172):
                    dipole_values[dipole_num].append(values[freq])
                    if freq % mhz_range == 0:
                        average = np.mean(dipole_values[dipole_num][len(dipole_values[dipole_num]) - mhz_range:len(
                            dipole_values[dipole_num]) + 1])
                        dipole_averages.append(round(average))
                dipole_num += 1
                # dipole_num is used here as a test has a length of 20 but we only want 16 dipoles from it.

        # Now we get the values for each 12Mhz over all dipoles
        for dipole in dipole_values:
            if dipole:
                for vals in range(mhz_range, len(dipole)):
                    if vals % mhz_range == 0:
                        overall_vals[count].append(dipole[vals - mhz_range: vals + 1])
                        count += 1
            count = 0

        # Now we get the average and standard deviation for each 12Mhz over all dipoles.
        for vals in range(len(overall_vals)):
            overall_average.append(np.mean(overall_vals[vals]))
            overall_std.append(np.std(overall_vals[vals]))

        # 110/10 = 11. So that we means we have 11 averages for each 10Mhz. For each dipole get its average, the
        # overall average and the standard deviation for all dipoles. We then subtract the std from the average as well
        # as a bit more which gives more or less sensitivity. Compare this to the dipole average and if it is under
        # then label that 10mhz as anomalous.
        dipole_num = 0
        for val in range(0, len(dipole_averages), 11):
            chunk = dipole_averages[val:val + 11]
            for v in range(0, mhz_range):
                std_upper_limit = round(overall_average[v] + overall_std[v] +
                                        app_config[os.getenv('APP_SETTINGS')].DIPOLE_DRIFT_SENSITIVITY)
                if chunk[v] > std_upper_limit:
                    dipole_anomalous[dipole_num].append(True)
                else:
                    dipole_anomalous[dipole_num].append(False)
            dipole_num += 1

        # We then see if a dipole will fall anomalous for the majority. If so then it is drifting, keeping in mind that
        # the sensitivity is a lot stricter than the lower/higher power anomalies.
        for idx, dipole in enumerate(dipole_anomalous):
            line_length = 0
            for dipole_val in dipole:
                if dipole_val:
                    line_length += 1
            if line_length > 9:
                anomaly = Anomaly()
                anomaly.observation = test[idx + 2].id
                anomaly.type = 10
                anomalies.append(anomaly)

        return anomalies

    def analyze_peak_stronger_on_x_than_y(self, x_polarity, y_polarity):
        """
        Analyze a test for the peak stronger on x than y anomaly. This is when the peaks between 240 - 280Mhz are
        higher on the x polarity compared to the y polarity.

        Args:
            test (test): A test for a specific tile.

       Returns:
            anomalies: An anomaly object representing the anomaly found with the x_polarity marked.
        """
        x_dipole_values = [[] for n in range(16)]
        y_dipole_values = [[] for n in range(16)]
        anomaly_list = []
        x_average = []
        y_average = []
        x_dipole_num = 0
        y_dipole_num = 0
        anomaly = None

        # Get the x and y values for each dipole
        for obs in x_polarity:
            values = json.loads(obs.values)
            if obs.name not in ("AllOff", "AllOn", "All_0"):
                for freq in range(190, 220):
                    x_dipole_values[x_dipole_num].append(values[freq])
                x_dipole_num += 1

        for obs in y_polarity:
            values = json.loads(obs.values)
            if obs.name not in ("AllOff", "AllOn", "All_0"):
                for freq in range(190, 220):
                    y_dipole_values[y_dipole_num].append(values[freq])
                y_dipole_num += 1

        # Get the averages for each dipole
        for x_dip in x_dipole_values:
            x_average.append(np.mean(x_dip))
        for y_dip in y_dipole_values:
            y_average.append(np.mean(y_dip))

        # Compare them and flag dipole as anomalous if X dipole > Y dipole
        for av in range(len(x_average)):
            if x_average[av] > y_average[av]:
                anomaly = Anomaly()
                anomaly.observation = x_polarity[av + 2].id
                anomaly.type = 4
                anomaly_list.append(anomaly)
        return anomaly_list

    def analyze_dipole_zeroes(self, test):
        """
        Analyze a test for the dipole containg zeroes anomaly. This is when a dipole has 0's all through its values.

        Args:
            test (test): A test for a specific tile.

        Returns:
            anomalies: A list of anomalies where each anomaly represents a dipole that contains 0s.
        """
        dipole_values = [[] for i in range(16)]
        dipole_num = 0
        anomalies = []

        # A dipole will contain all 0's if it fails to read from the database.
        for obs in test:
            if obs.name not in ("AllOn", "AllOff", "All_0"):
                values = json.loads(obs.values)
                for freq in range(1, 255):
                    if values[freq] is 0:
                        dipole_values[dipole_num].append(0)
                dipole_num += 1

        for idx, dipole in enumerate(dipole_values):
            if len(dipole) == 255:
                anomaly = Anomaly()
                anomaly.observation = test[idx + 2]
                anomaly.type = 11
                anomalies.append(anomaly)

        return anomalies

    def fix_noise(self, test):
        """
        Fix noise in 115-145Mhz range.

        Args:
            test (test): A test for a specific tile.

        Returns:
            test (test): A test for a specific tile with noise fixed.
        """
        for obs in test.observations:
            if obs.name not in ("AllOff", "AllOn", "All_0"):
                values = json.loads(obs.values)
                for freq in range(79, 112):
                    if values[freq] > 30:
                        average = (values[freq - 1] + values[freq - 2]) / 2
                        values[freq] = average
                obs.values = str(values)
        return test
