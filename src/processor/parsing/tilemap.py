"""
Library to fetch the receiver/slot to tilename mapping information from the MWA configuration
   web service.

   EG:
   >>> import tilemap
   >>> rdict, tdict = tilemap.get_con_dicts(ref_time=1188199410)
   >>> tileid, tilename, flagged = rdict[(13, 3)]
   >>> print "Receiver 13 slot 3 is tile number %d, named %s, and Flagged=%s" % (tileid, tilename, flagged)
   Receiver 13 slot 3 is tile number 1003, named HexE3, and Flagged=None
   >>> receiver_id, slot, flagged = tdict[24]
   >>> print "Tile number 24 is on receiver %d, slot %d, and flagged=%s" % (receiver_id, slot, flagged)
   Tile number 24 is on receiver 14, slot 4, and flagged=None

   NOTE: 1 based indexing
"""

import json
import urllib
import urllib2

_BASEURL = 'http://mwa-metadata01.pawsey.org.au'


def fetch_metadata(service='obs', gps_time=None, filename=None, URL=_BASEURL):
    """Given a service (eg 'obs' or 'con') and either a gps_time in
    seconds or a data file name for that observation, return
    the metadata as a Python dictionary.
    """
    if gps_time is not None:
        data = urllib.urlencode({'obs_id': gps_time})
    elif filename is not None:
        data = urllib.urlencode({'filename': filename})
    else:
        print "Must pass either filename or obs_id"
        return

    if service.strip().lower() in ['obs', 'con']:
        service = service.strip().lower()
    else:
        print "Invalid service name: %s" % service
        return

    url = URL + '/metadata/' + service + '?' + data
    try:
        result = json.load(urllib2.urlopen(url))
    except urllib2.HTTPError as error:
        print "HTTP error from server: code=%d, response:\n %s" % (error.code, error.read())
        print 'Unable to retrieve %s' % url
        return
    except urllib2.URLError as error:
        print "URL or network error: %s" % error.reason
        print 'Unable to retrieve %s' % url
        return
    except:
        print 'Unable to retrieve %s' % url
        return

    return result


def get_con_dicts(ref_time=None):
    """Given a reference time in GPS seconds, query the MWA metadata web service to return the telescope configuration as
       at the time specified.

       This function returns two dictionaries. The first has tuples of (receiver_id, slot) as keys, and tuples of (tile_id, tilename, flagged)
       as values. The second has tile_id as the keys, and tuples of (receiver_id, slot, flagged) as values.

       Here:
        - receiver_id is an integer from 1 to 16
        - slot ranges is an integer from 1-8
        - tile_id is an integer (eg, 11-18, 21-28, ... 161-168, 1001-1072, etc)
        - tilename is a string (eg 'Tile021' or 'HexE23')
        - flagged is a boolean, True if the tile is defective for some reason OTHER than having too many bad dipoles.
    """
    if not ref_time:
        print "ERROR - Must pass a time in GPS seconds to tilemap.get_con_dicts()"
        return None, None

    con = fetch_metadata(service='con', gps_time=ref_time)

    receiver_dict = {}
    tile_dict = {}
    for tidstring, tile in con.items():
        tile_id = tile['id']
        tile_name = str(tile['name'])  # convert from unicode to ascii string
        rec_id = tile['receiver']
        slot = tile['slot']
        flagged = tile['flagged']
        receiver_dict[(rec_id, slot)] = (tile_id, tile_name, flagged)
        tile_dict[tile_id] = (rec_id, slot, flagged)
    return receiver_dict, tile_dict
