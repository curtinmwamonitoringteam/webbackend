"""
USAGE:  $ python
		>>> from processor.parsing.dptest import load_file
		>>> sample = '../../test/sample.pickle'
        >>> alldata, dstate = load_file(sample)

This will load a single pickled dipole health data set. The 'dstate' is an instance of the
DipoleState class defined below, representing which dipoles on which tiles have been flagged
by a human as 'bad' at the time of that test. Ignore this for now, because you don't (yet)
have the code to map between the tile ID (in the dstate object) and the receiver/slot IDs in
the 'alldata' object.

The 'alldata' object is a complicated structure. It is a Python list with 20 elements, corresponding
to the 20 observations in the short dipole test. In order, these are 'All_On', 'All_Off', 'Dipole A',
'Dipole B', ... 'Dipole P', 'All_Off', 'All_On'. The sixteen individual dipoles on the tile are labelled
A through P, and the others are observations where all of the dipoles were turned on, or all turned off
(taken for comparison, to see if the beamformer was pointing).

Each of the 20 elements, one for each observation, is a tuple of (starttime, obsname, data), where
'starttime' is the time in GPS seconds (a 10 digit integer), 'obsname' is a string with the name of
the observation, and 'data' is a numpy array containing the actual power data.

Each of the 'data' elements is a four dimensional array of float32 values. The first axis is
the receiver number (minus 1) (0-15 for the 16 airconditioned boxes in the field), the second axis is the
slot number (minus 1) in that receiver (0-7 for the eight tiles handled by a receiver). These two together define
which physical tile is being measured. The third axis is polarisation (0 for X, 1 for Y). The fourth axis
is frequency channel (0-255, where frequency is 1.28 MHz times the channel number).

To convert the power levels in the data structure into decibels for the plots, we plot
10 * numpy.log10(data + 1).


"""

import cPickle
import pickle
import numpy as np
import importlib
import sys
import warnings
import csv
import pandas as pd
from scipy.stats._continuous_distns import halfcauchy_gen

from sklearn.model_selection import train_test_split
from sklearn import neighbors, linear_model
from sklearn import utils
from sklearn import datasets
from sklearn import preprocessing
from sklearn import svm
from sklearn import metrics


class Dipole(object):
    """Class to represent a single dipole"""

    def __init__(self, code=None, tileid=None, dipoleid=None, pol='X', comment=''):
        """Here, tileid is a positive integer, dipoleid is EITHER a number from 1-16 OR a letter from A-P,
           and pol is either 'X' or 'Y'.

           If 'code' is given, it should be a string encoding all of tileid, dipoleid, and pol, and if valid, it
           overrides all the other parameters. Typical codes are '34DX', '3PY', etc - a one to three digit tile ID,
           followed by a dipole ID as a letter from A to P, followed by either X or Y. Whitespace is ignored.
        """
        self.valid = True
        if code is not None:  # Parse the code and ignore the other arguments
            code = "".join(code.split())  # remove any whitespace
            if len(code) >= 3:
                ts = code[:-2]
                if ts.isdigit():
                    tileid = int(ts)
                dipoleid = code[-2:-1]
                pol = code[-1]

        if (type(tileid) == int) and (tileid > 0):
            self.tileid = tileid
        else:
            self.tileid = None
            self.valid = False

        if (type(dipoleid) == int) and (dipoleid > 0) and (dipoleid < 17):
            self.dipoleid = chr(dipoleid + 64)
        elif (type(dipoleid) == str) and (dipoleid.upper() >= 'A') and (dipoleid.upper() <= 'P'):
            self.dipoleid = dipoleid.upper()
        else:
            self.dipoleid = None
            self.valid = False

        if (type(pol) == str) and (pol.upper() in ['X', 'Y']):
            self.pol = pol.upper()
        else:
            self.pol = None
            self.valid = False

        self.comment = comment

    def __repr__(self):
        if self.valid:
            return "<Dipole(tileid=%s, dipoleid='%s', pol='%s', comment='%s')>" % (
                self.tileid, self.dipoleid, self.pol, self.comment)
        else:
            return "<Invalid Dipole(tileid=%s, dipoleid='%s', pol='%s', comment='%s')>" % (
                self.tileid, self.dipoleid, self.pol, self.comment)

    def __str__(self):
        if self.valid:
            return "T%s %s-%s" % (self.tileid, self.dipoleid, self.pol)
        else:
            return "*T%s %s-%s*" % (self.tileid, self.dipoleid, self.pol)


class DipoleState(object):
    def __init__(self, name=None, reftime=None, db=None):
        self.dipoles = {}  # Dictionary, with tileid as the key
        self.reftime = reftime

        if not name:
            self.name = 'default'
        else:
            self.name = name.lower()

    def __repr__(self):
        return "<DipoleState(name='%s', reftime=%d)>" % (self.name, self.reftime)

    def __str__(self):
        res = "Set '%s' at reftime=%d:\n" % (self.name, self.reftime)
        tlist = self.dipoles.keys()
        tlist.sort()

        for t in tlist:
            dplist = [str(d) for d in self.dipoles[t]]
            res += "  %s\n" % ', '.join(dplist)

        return res

    def get_tile(self, tileid):
        """Return the X and Y exclusion lists for the specified tile ID, as sorted lists of numeric dipole ID's,
           to match the format in the database.
        """
        xlist = []
        ylist = []
        for d in self.dipoles[tileid]:
            if d.pol == 'X':
                xlist.append(ord(d.dipoleid) - 64)
            else:
                ylist.append(ord(d.dipoleid) - 64)
        xlist.sort()
        ylist.sort()

        return xlist, ylist

    def save(self, begintime=None, db=None):
        """Save Dipole State object to database.
        """
        pass


def find_global(module_name, class_name):
    if module_name == 'obssched.dipole' and class_name == 'DipoleState':
        return DipoleState

    if module_name == 'obssched.dipole' and class_name == 'Dipole':
        return Dipole
    else:
        mod = importlib.import_module('numpy.core.multiarray')
        return getattr(mod, class_name)


def load_file(fname=''):
    """Takes the name of a pickled dipole health data file (NNNNNNNNNN.pickle), and
       returns the contents as a tuple of (alldata, dstate), where
    """
    f = open(fname, 'rb')
    unpickler = cPickle.Unpickler(f)
    unpickler.find_global = find_global
    alldata, dstate = unpickler.load()

    return alldata, dstate


def load():
    """Get the list of tests for 1154042456"""
    all_data, dstate = loadfile('../../../upload/2016-08-01 07:20:39 1154042456.pickle')

    return all_data


def load2():
    """Get the list of tests for 1154261080"""
    all_data, dstate = loadfile('../../../upload/2016-08-03 20:04:23 1154261080.pickle')

    return all_data


class Observation:
    """Represents a mock observation in order to mock the database"""
    values = []

    def __init__(self):
        self.name = None
        self.receiver = None
        self.slot = None
        self.polarity = None
        self.values = []


class Test:
    """Represents a mock test in order to mock the database"""
    observations = []

    def __init__(self):
        self.receiver = None
        self.slot = None
        self.polarity = None
        self.observations = []


def make(data):
    """Parse the all_data object, mocking the database parsing"""
    test_list = []

    # create the list
    for receiver in range(16):
        for slot in range(8):
            test = Test()
            test_list.append(test)

            # get associated tile_id
            test.slot = slot
            test.receiver = receiver

            # build observations
            for obs_no in range(20):
                for polarity in range(2):
                    test.polarity = polarity
                    obs = Observation()

                    # get name
                    obs.name = data[obs_no][1]

                    # get correct dipole_id
                    obs.slot = slot
                    obs.receiver = receiver
                    obs.polarity = polarity

                    # get values (one for each frequency channel)
                    values = []

                    for channel in range(256):
                        dipole = data[obs_no][2]
                        values.append(float(dipole[receiver][slot][polarity][channel]))

                    obs.values = values

                    # add the observation to the test
                    test.observations.append(obs)

    return test_list


def parse():
    """Loads files, parses to test list and iterates through them"""
    all_data = load()
    d_list = make(all_data)

    all_data2 = load2()
    d_list2 = make(all_data2)

    # Gives stack trace on numpy exceptions
    warnings.simplefilter("error")

    averages = [[] for i in range(128)]
    averages2 = [[] for i in range(128)]

    for idx, test in enumerate(d_list):
        if (test.receiver is 6 and test.slot is 0):
            fixed = fix_noise(test)
            x_polarity = fixed.observations[0:len(fixed.observations):2]
            y_polarity = fixed.observations[1:len(fixed.observations):2]
            averages[idx].append(machine(x_polarity))
            # averages[idx].append(machine(y_polarity))
            # makeCSVFile(x_polarity)
            # test_values(x_polarity)

    for idx, test in enumerate(d_list):
        fixed = fix_noise(test)
        y_polarity = fixed.observations[1:len(fixed.observations):2]
        averages2[idx].append(machine(y_polarity))

    return averages + averages2


def test_values(test):
    """Iterate through observations and print them. Use this to get specific tile data."""
    vals = []

    for obs in test:
        if obs.name not in ("AllOn", "All_0"):
            for freq in range(1, 255):
                vals.append(obs.values[freq])
                print ", %d" % obs.values[freq],

            print ""
            average = np.mean(vals)
            # print "test %f" %average


def fix_noise(test):
    """Fix noise in 115-145Mhz range"""

    for obs in test.observations:
        if obs.name not in ("AllOff", "AllOn", "All_0"):
            for freq in range(79, 112):
                if obs.values[freq] > 30:
                    average = (obs.values[freq - 1] + obs.values[freq - 2]) / 2
                    obs.values[freq] = average

    return test


def machine(test):
    """"Gets an average over all dipoles for each 12Mhz"""
    dipole_values = [[] for i in range(16)]
    dipole_averages = []
    overall_vals = [[] for i in range(10)]
    overall_average = []
    overall_std = []
    dipole_num = 0
    count = 0
    mhz_range = 10

    # First we calculate the average for each ~12Mhz (10 Channels = 12Mhz) for each dipole
    for obs in test:
        if obs.name not in ("AllOn", "AllOff", "All_0"):
            for freq in range(61, 162):
                dipole_values[dipole_num].append(obs.values[freq])
                if freq % mhz_range == 0:
                    average = np.mean(dipole_values[dipole_num][len(dipole_values[dipole_num]) - mhz_range:len(
                        dipole_values[dipole_num]) + 1])
                    dipole_averages.append(round(average))
            dipole_num += 1

    for dipole in dipole_values:
        if dipole:
            for vals in range(mhz_range, len(dipole)):
                if vals % mhz_range == 0:
                    overall_vals[count].append(dipole[vals - mhz_range: vals + 1])
                    count += 1
        count = 0

    for vals in range(len(overall_vals)):
        overall_average.append(np.mean(overall_vals[vals]))

    return overall_average


def ml_rainbow():
    """Use the KNN algorithm to detect the rainbow effect anomaly."""
    old = parse()

    # Rainbow effect data
    anonz = [[] for i in range(6)]
    test_one = [145.72488747943532, 179.41934793645686, 198.45430313321677, 198.45521297525821, 198.45521386377584,
                550.08511875614852, 620.72471688010478, 565.83243474093354, 425.3284580924294, 303.82865619659424,
                212.69644438136709]
    test_two = [55.174020832235165, 79.271832276474342, 89.278164110638585, 89.28576155911243, 89.28576897849571,
                286.45772374758957, 343.37123628096145, 339.31929423592305, 266.33272899280894, 191.19908480210736,
                135.64980012720281]

    test_three = [85.646402835845947, 128.48763891241768, 147.15354800436498, 147.16908954293675, 147.16910472022053,
                  427.57385333376936, 509.85236289284444, 481.73680357499558, 366.64388535239482, 255.29616962779653,
                  174.66810594905508]

    test_four = [97.910619302229449, 144.74015992879868, 165.92845065504926, 165.94865842217027, 165.94867815631784,
                 449.79015100360704, 515.14658754522145, 469.19120840592819, 343.30749823830342, 234.82893163507634,
                 163.03214992176402]

    test_five = [59.812809228897095, 94.183452546596527, 107.7889057737828, 107.79266657369271, 107.79267024634891,
                 338.20965272729524, 419.74092812971634, 424.10375092246318, 339.86831240220505, 236.85617438229647,
                 164.05571113933217]

    # These two arrays are to test the model to see if it is predicting correctly.
    test_six = [125, 125, 125, 125, 125, 125, 125, 125, 125, 125, 125]

    test_seven = [35] * 11

    anonz[0].append(test_one)
    anonz[1].append(test_two)
    anonz[2].append(test_three)
    anonz[3].append(test_four)
    anonz[4].append(test_five)
    anonz[5].append(test_six)
    averages = anonz + old
    xx = np.array([np.array(xi) for xi in averages])
    anon = []

    # Label the data anomalous if its average is over 30
    for a in averages:
        if np.mean(a) >= 30:
            anon.append(-1)
        else:
            anon.append(1)

    y = np.array(anon)
    # Array needs to be 2D.
    x = xx.reshape(262, 11)

    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=42)

    knn = neighbors.KNeighborsClassifier(n_neighbors=1)
    model = knn.fit(X_train, y_train)

    print('score: %d' % model.score(X_test, y_test))

    # See if the algorithm will correctly predict the anomalous data is anomalous.
    class_data = np.array(test_seven)
    class_data = class_data.reshape(1, -1)

    print(model.predict(class_data))


def makeCSVFile(test_list):
    """
    Create a CSV file with the observation values as its values and append 1
    representing normal or -1 representing anomalous
    """
    with open('data.csv', 'a') as csvfile:
        filewriter = csv.writer(csvfile, delimiter=',')

        for test in test_list:
            fixed = fix_noise(test)
            x_polarity = fixed.observations[0:len(fixed.observations):2]
            averages = machine(x_polarity)

            for obs in x_polarity:
                if obs.name not in ("AllOn", "All_0", "AllOff"):
                    if np.mean(averages) >= 20:
                        row = obs.values + [-1]
                        filewriter.writerow(row)
                    else:
                        row = obs.values + [1]
                        filewriter.writerow(row)


def loadCSV():
    """Load pickle files and create csv's from them"""
    all_data = load()
    d_list = make(all_data)

    all_data2 = load2()
    d_list2 = make(all_data2)
    makeCSVFile(d_list)
    makeCSVFile(d_list2)


def rainbow_ML_CSV_KNN():
    """Uses the KNN algorithm to read a csv, create a model and get its accuracy for the rainbow anomaly"""

    # Append labels to each column where the last column denotes if the row is anomalous or not.
    names_int = range(1, 255)
    names = ""
    names = [str(x) for x in names_int]
    names.append("anon")

    # Load data into pandas dataframe
    df = pd.read_csv('../../../upload/rainbow_knn.csv', header=None, names=names)

    # Convert data to Np arrays as required where x is the data to be analyzed and y is the anomalous labels
    x = np.array(df.ix[:, 1:256])
    y = np.array(df['anon'])

    # Split data into training and test data
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=42)
    knn = neighbors.KNeighborsClassifier(n_neighbors=1)

    # Fit model to data
    model = knn.fit(x_train, y_train)

    # Write to model to file for future loading
    filename = 'rainbow_ml_knn.sav'
    pickle.dump(model, open(filename, 'wb'))

    # Use new data to see if it will predict correctly
    test_seven = [23] * 254
    class_data = np.array(test_seven)
    class_data = class_data.reshape(1, -1)

    print "Rainbow anomaly using KNN | 1 = not anomalous | -1 = anomalous"
    print "Predict against new anomalous data %d" % model.predict(class_data)
    print "Get overall accuracy between 0 and 1"
    print (metrics.accuracy_score(y_train, model.predict(x_train)))
    print ""


def rainbow_ML_SVM():
    """Uses the SVM algorithm to read a csv, create a model and get its accuracy for the rainbow anomaly"""

    # Append labels to each column where the last column denotes if the row is anomalous or not.
    names_int = range(1, 255)
    names = ""
    names = [str(x) for x in names_int]
    names.append("anon")

    # Load data into pandas dataframe
    df = pd.read_csv('../../../upload/rainbow_knn.csv', header=None, names=names)

    # SVM requires normalized data so normalize all of it into 0..1
    x = df.values
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(x)
    df = pd.DataFrame(x_scaled, columns=names)

    # Get all anomalous labels
    zeros = df['anon'] == 0
    df.loc[zeros, 'anon'] = 0

    # Get all anomalous and not anomalous readings
    target = df['anon']
    anomalous = target[target == 0]

    # SVM is unsupervised so it does not require labeled data
    df.drop('anon', axis=1)

    # Split data into training and test data
    x_train, x_test, y_train, y_test = train_test_split(df, target, train_size=0.8)

    # Fit model to data
    model = svm.OneClassSVM(nu=0.10, kernel='rbf', gamma=0.1)
    model.fit(x_train)

    # Write to model to file for future loading
    filename = 'rainbow_ml_svm.sav'
    pickle.dump(model, open(filename, 'wb'))

    # Use new data to see if it will predict correctly
    print "Get overall accuracy(between 0 and 1) for rainbow effect anomaly using SVM"
    print(metrics.accuracy_score(y_train, model.predict(x_train)))
    print ""


def clustered_ML_SVM():
    """Uses the SVM algorithm to read a csv, create a model and get its accuracy for the clustered lines anomaly"""

    # Append labels to each column where the last column denotes if the row is anomalous or not.
    names_int = range(1, 255)
    names = ""
    names = [str(x) for x in names_int]
    names.append("anon")

    # Load data into pandas dataframe
    df = pd.read_csv('../../../upload/clustered.csv', header=None, names=names)

    # SVM requires normalized data so normalize all of it into 0..1
    x = df.values
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(x)
    df = pd.DataFrame(x_scaled, columns=names)

    # Get all anomalous labels
    zeros = df['anon'] == 0
    df.loc[zeros, 'anon'] = 0

    # Get all anomalous and not anomalous readings
    target = df['anon']
    anomalous = target[target == 0]

    # SVM is unsupervised so it does not require labeled data
    df.drop('anon', axis=1)

    # Split data into training and test data
    x_train, x_test, y_train, y_test = train_test_split(df, target, train_size=0.8)

    # Fit model to data
    model = svm.OneClassSVM(nu=0.10, kernel='rbf', gamma=0.1)
    model.fit(x_train)

    # Write to model to file for future loading
    filename = 'clustered_ml_svm.sav'
    pickle.dump(model, open(filename, 'wb'))

    # Use new data to see if it will predict correctly
    print "Get overall accuracy (between 0 and 1) for clustered lines anomaly using SVM"
    print(metrics.accuracy_score(y_train, model.predict(x_train)))
    print ""


def clustered_ML_KNN():
    """Uses the KNN algorithm to read a csv, create a model and get its accuracy for the clustered lines anomaly"""

    # Append labels to each column where the last column denotes if the row is anomalous or not.
    names_int = range(1, 255)
    names = ""
    names = [str(x) for x in names_int]
    names.append("anon")

    # Load data into pandas dataframe
    df = pd.read_csv('../../../upload/clustered.csv', header=None, names=names)

    # Convert data to Np arrays as required where x is the data to be analyzed and y is the anomalous labels
    x = np.array(df.ix[:, 1:256])
    y = np.array(df['anon'])

    # Split data into training and test data
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=42)

    # Fit model to data
    knn = neighbors.KNeighborsClassifier(n_neighbors=1)
    model = knn.fit(x_train, y_train)

    # Write to model to file for future loading
    filename = 'clustered_ml_knn.sav'
    pickle.dump(model, open(filename, 'wb'))

    # Use new data to see if it will predict correctly
    test_seven = [25] * 254
    class_data = np.array(test_seven)
    class_data = class_data.reshape(1, -1)

    print "Rainbow anomaly using KNN | 1 = not anomalous | -1 = anomalous"
    print "Predict against new normal data %d" % model.predict(class_data)
    print "Get overall accuracy between 0 and 1"
    print metrics.accuracy_score(y_train, model.predict(x_train))
    print ""


# parse()
# loadCSV()
# ml_rainbow()
# rainbow_ML_CSV_KNN()
# rainbow_ML_SVM()
# clustered_ML_SVM()
# clustered_ML_KNN()
