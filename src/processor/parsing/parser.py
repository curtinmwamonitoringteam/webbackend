import json

from database.model import Test, Observation
from processor.parsing import tilemap
from repository import dipole_repository as dipole_repo
from repository import ignore_tile_repository as ignore_tile_repo


def parse_all_data(all_data):
    """
    Iterates over all_data and returns a list of Test objects.

    Args:
        all_data (list): List of 20 elements corresponding to 20 observations within a short dipole test.

    Returns:
        test_list (list): A list of test objects.
    """
    test_list = []
    ignored_tile_ids = [x.tile_id for x in ignore_tile_repo.get_all()]

    # Create the list
    for receiver in range(16):
        for slot in range(8):
            test = Test()

            # Get the earliest start time for the test
            test.start_time = all_data[0][0]

            # Get tile mappings for this test
            receiver_dict, tile_dict = tilemap.get_con_dicts(ref_time=test.start_time)

            # Get associated tile_id
            test.tile_id = receiver_dict[(receiver + 1, slot + 1)][0]

            # Ignore flagged tiles
            if test.tile_id in ignored_tile_ids:
                continue

            test_list.append(test)

            # Build observations
            for obs_no in xrange(len(all_data)):
                for polarity in range(2):
                    obs = Observation()

                    # Get start time
                    obs.start_time = all_data[obs_no][0]

                    # Get name
                    obs.name = all_data[obs_no][1]

                    # Get correct dipole_id
                    obs.dipole_id = dipole_repo.get_dipole_id(test.tile_id, obs_no, polarity)

                    # Get values (one for each frequency channel)
                    # The first value, index 0, is omitted. Legacy value no longer needed.
                    values = []
                    data = all_data[obs_no][2]

                    for channel in range(1, 256):
                        values.append(float(data[receiver][slot][polarity][channel]))

                    obs.values = json.dumps(values)

                    # Add the observation to the test
                    test.observations.append(obs)

    return test_list
